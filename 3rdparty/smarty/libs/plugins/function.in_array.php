<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */



function smarty_function_in_array($string, $array)
{
    libraryLog::_getInstance() -> warn('string: '.$string, $array);
    return in_array($string, $array);
}

?>