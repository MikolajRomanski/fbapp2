<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * Type:     modifier<br>
 * Name:     base64_encode<br>
 * Date:     Dec 07, 2011
 * Purpose:  Encoduje string za pomocą base64_encode
 * Input:<br>
 *         string
 * Example:  {$text|base64_encode}
 * @version  1.0
 * @author   Mikołaj Romański
 * @param string
 * @return string
 */
function smarty_modifier_base64_encode($string)
{
    return base64_encode($string);
}

/* vim: set expandtab: */

?>
