<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_humansize($string)
{
    return Files::getfilesize($string);
}

/* vim: set expandtab: */

?>
