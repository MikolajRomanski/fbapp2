<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty make_url modifier plugin
 *
 * Type:     modifier<br>
 * Name:     make_url<br>
 * Purpose:  Przekształca string na bezpieczny url
 * @author   Mikołaj Romański <mikolaj@doromi.net>
 * @param string
 * @return string
 */
function smarty_modifier_make_url($string)
{
    return Translate::make_url($string);
}

?>
