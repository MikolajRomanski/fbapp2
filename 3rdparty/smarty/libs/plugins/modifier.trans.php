<?php
function smarty_modifier_trans($string)
{
    $trans = string_translate::getInstance();
    return $trans ->translate($string);
}
?>