<?php
class Translate{
	public static function make_url($str)
	{
                $char_in_ru = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'юu', 'я');
                $char_out_ru = array('A', 'B', 'V', 'G', 'D', 'E', 'ZH', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'TS', 'CH', 'SH', 'SCH', '', 'Y', '', 'E', 'YU', 'YA', 'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ya');
                $str= str_replace($char_in_ru,$char_out_ru,$str);
                
		$char_in = array('ą','ć','ę','ł','ń','ó','ś','ź','ż','Ą','Ć','Ę','Ł','Ń','Ó','Ś','Ź','Ż','?','!', ' ', ',', '"', '\'','.','\\','/');
		$char_out = array('a','c','e','l','n','o','s','z','z','A','C','E','L','N','O','S','Z','Z','','', '-', '', '', '','-','-','-');
                
		$in= str_replace($char_in,$char_out,$str);
		return $out=preg_replace('#[^a-z0-9]+#i','-',$in);
	}
	public static function make_filename($str)
	{
		$char_in_ru = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'юu', 'я');
                $char_out_ru = array('A', 'B', 'V', 'G', 'D', 'E', 'ZH', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'TS', 'CH', 'SH', 'SCH', '', 'Y', '', 'E', 'YU', 'YA', 'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ya');
                $str= str_replace($char_in_ru,$char_out_ru,$str);
            
                $char_in = array('ą','ć','ę','ł','ń','ó','ś','ź','ż','Ą','Ć','Ę','Ł','Ń','Ó','Ś','Ź','Ż','?','!', ' ', ',', '"', '\'','\\','/');
		$char_out = array('a','c','e','l','n','o','s','z','z','A','C','E','L','N','O','S','Z','Z','','', '_', '', '', '','_','_');
                
		$in= str_replace($char_in,$char_out,$str);
		return $out=preg_replace('#[^a-z0-9\.\-]+#i','_',$in);
	}
	public static function make_table_name($str)
	{
		$str = strtolower($str);
		$char_in = array('ą','ć','ę','ł','ń','ó','ś','ź','ż','Ą','Ć','Ę','Ł','Ń','Ó','Ś','Ź','Ż','?','!', ' ', ',', '"', '\'','.','\\','/');
		$char_out = array('a','c','e','l','n','o','s','z','z','A','C','E','L','N','O','S','Z','Z','','', '_', '', '', '','_','_','_');
		$in= str_replace($char_in,$char_out,$str);
		return $out=preg_replace('#[^a-z0-9]+#i','_',$in);
	}
	
	public function clear_string($str){
		$char_in = array('&','<','>');
		$char_out = array(' ',' ',' ');
		$in= str_replace($char_in,$char_out,$str);
		return $out=preg_replace('#[^a-z0-9]{!}+#i',' ',$in);
	}
	public function unsafe($str)
	{
		return get_magic_quotes_gpc() ? stripslashes($str) : $str;
	}
	public static function make_safe($str)
	{
		return get_magic_quotes_gpc() ?  $str : addslashes($str);
	}
	public static function make_safe_array_post($element){
                

                $el = array();
                foreach($element as $key => $value){
                    $el[addslashes($key)] = !is_array($element[$key]) ? addslashes($value) : self::make_safe_array_post($value);
                }

		return $el;
	}
	public function make_unsafe_array_post($key,$item){
		return $_POST[$item]=stripslashes($key);
	}
	public static function make_safe_array_get($element){
		//return get_magic_quotes_gpc() ?  $_GET[$item]=$key : $_GET[$item]=addslashes($key);
                $el = array();
                    foreach($element as $key => $value){
                        $el[addslashes($key)] = !is_array($element[$key]) ? addslashes($value) : self::make_safe_array_get($value);
                    }

                    return $el;
            
	}
	public function make_unsafe_array_get($key,$item){
		return $_GET[$item]=stripslashes($key);
	}

	public static function magic_quotes()
	{
		libraryLog::getinstance() -> setinfo('Tablica _GET: ',$_GET);
		libraryLog::getinstance() -> setinfo('Tablica _POST: ',$_POST);
		
		if(function_exists('get_magic_quotes_gpc'))
		{
			if(get_magic_quotes_gpc())
			{
				
				libraryLog::getinstance() -> setwarning("magic_quotes",'WŁĄCZONE');
												
			}
			else
			{
				libraryLog::getinstance() -> setinfo("magic_quotes",'Wyłączone - bardzo dobrze');
				
				//array_walk_recursive($_POST,array("Translate","make_safe_array_post"));
                                $_POST = Translate::make_safe_array_post($_POST);
                                $_GET = Translate::make_safe_array_get($_GET);
				
				
				libraryLog::getinstance() -> setinfo('MODYFIKACJA _GET: ',$_GET);
				libraryLog::getinstance() -> setinfo('MODYFIKACJA _POST: ',$_POST);
				
			}
		}
		else
		{
			libraryLog::getinstance() -> setinfo("magic_quotes","Doesn't exists - good :-)");
			
			$_POST = Translate::make_safe_array_post($_POST);
                        $_GET = Translate::make_safe_array_get($_GET);
			
			libraryLog::getinstance() -> setinfo('MODYFIKACJA _GET: ',$_GET);
			libraryLog::getinstance() -> setinfo('MODYFIKACJA _POST: ',$_POST);
		}
		
	}

        public static function check_numberformat($value, $points, $langid){
            switch($langid){
                case 'Raw':
                    libraryLog::getinstance()-> setinfo('Translate checknumber', 'RAW');
                    $number = str_replace(',', '.',str_replace(array('.', ' '), '', $value));
                   return $number;
                break;
                default:

                    $number = str_replace(',','.',str_replace(array('.', ' '), '', $value));
                    return number_format($number, $points, ",", " ");

                break;
            }
        }
}
?>