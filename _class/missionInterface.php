<?php
interface missionInterface{
    public function join( $userid );
    public function missionInformations( $string );
    public function getTemplate( $method );
    public function makeAction( $method );
}
?>