<?php

class Database {

    /**
     *
     * @var PDO
     */
    private $pdo = null;

    /**
     *
     * @var Benchmark
     */
    private $benchmark;

    /**
     *
     * @var libraryLog
     */
    private $log = null;
    private $count = 0;
    private $dbhost = null;
    private $dbport = null;
    private $dbname = null;
    private $dbuser = null;
    private $dbpass = null;
    /*
     * Ostatnio dodany wiersz
     */
    private $lastInsertId = null;
    private $lastInsertOid = null;
    /*
     * Ilość wierszy zapytania
     */
    private $rowsCount = 0;
    private $lastError = null;
    private $debugstatus = true;
    
    public function __construct() {
        $this->benchmark = Benchmark::_getInstance();
        $bid = $this->benchmark->Start('db connection');
        $this->log = libraryLog::_getInstance();
        if (empty($this->dbhost)) {
            $this->dbhost = DB_HOST;
            $this->dbport = DB_PORT;
            $this->dbname = DB_NAME;
            $this->dbuser = DB_USER;
            $this->dbpass = DB_PASS;
        }
        try {

            $this->pdo = new PDO(PDODRIVER.':host=' . $this->dbhost . ';dbname=' . $this->dbname . ';port=' . $this->dbport, $this->dbuser, $this->dbpass);
        } catch (PDOException $e) {
            libraryLog::_getInstance()->error('Połączenie nie mogło być nawiązane', $e);
            die('connectionerror');
        }
        if(PDODRIVER=='mysql') $this->pdo->exec("SET CHARACTER SET utf8");
        $this->benchmark->End($bid);
    }

    /**
     * Pobierz instację połączenia bazy danych
     * @author Mikołaj Romański <mikolaj@doromi.net>
     * @return Database
     */
    public static function _getInstance($dbhost = null, $dbport = null, $dbname = null, $dbuser = null, $dbpass = null) {
        static $_inst = null;
        if ($_inst == null) {
            $_inst = new Database();
        }
        return $_inst;
    }

    private static function _connect($dbhost = null, $dbport = null, $dbname = null, $dbuser = null, $dbpass = null) {
        $instance = new Database();
        $instance->make_connection($dbhost, $dbport, $dbname, $dbuser, $dbpass);


        return $instance;
    }

    private function make_connection($dbhost, $dbport, $dbname, $dbuser, $dbpass) {
        try {
            $this->pdo = new PDO('mysql:host=' . $dbhost . ';dbname=' . $dbname . ';port=' . $dbport, $dbuser, $dbpass);
        } catch (PDOException $e) {

            die('connection error' . $e . "\n");
        }
    }

    /**
     * Pobierz wiersz wyników
     * @author Mikołaj Romański <mikolaj@doromi.net>
     * @param string $q
     * @return array
     */
    public function getArray($q, $benchmark = true) {
        if ($benchmark)
            $bid = $this->benchmark->Start($q);
        $this->rowsCount = 0;
        $query = trim($q);
        if (empty($q)) {
            if($this -> debugstatus) $this->log->warn('Pytanie było puste', $q);
            return '';
        }

        try {
            $odp = $this->pdo->query($q);
            if (empty($odp)) {
                if ($benchmark)
                    $this->benchmark->End($bid);
                $errcode = $this->pdo->errorInfo();
                $this->log->error($q, $errcode['2']);
                return false;
            }
            $dane = $this->merge_as_array($odp);
            $odp->closeCursor();
        } catch (PDOException $e) {
            libraryLog::_getInstance()->error($q, $e);
            if ($benchmark)
                $this->benchmark->End($bid);
            return false;
        }
        if ($benchmark)
            $this->benchmark->End($bid);
        return $dane;
    }

    public function insertSQL($array, $table) {
        $bid = $this->benchmark->Start("INSERT SQL");
        $q = "select column_name,data_type,character_maximum_length from INFORMATION_SCHEMA.COLUMNS where table_name = '$table'";
        $arr = $this->getArray($q);
        $cols = array();
        foreach ($arr as $value) {
            $cols[] = $value['column_name'];
        }
        if($this -> debugstatus) $this->log->info('Kolumny', $cols);
        if($this -> debugstatus) $this->log->info('Insert', $array);
        $binds = array();
        foreach ($array as $key => $value) {
            if (!in_array($key, $cols))
                continue;
            $keys[] = $key;
            $dane['bind'] = ":$key";
            $dane['value'] = $value;
            $dane['type'] = $this->getTypeOf($key, $arr);
            $binds[] = $dane;
        }
        if($this -> debugstatus) $this->log->warn('BINDS', $binds);
        $keys_ = implode(", ", $keys);
        $keys__ = implode(", :", $keys);


        $q = "INSERT INTO $table ( $keys_ ) VALUES (:$keys__)";
        $this->benchmark->Modify($bid, $q);
        $statment = $this->pdo->prepare($q);
        foreach ($binds as $value) {
            $statment->bindValue($value['bind'], $value['value'], $value['type']);
        }
        $ok = $statment->execute();
        //$this -> log ->warn('after execute', $ok);
        if (!$ok) {
            $this->lastError = $statment->ErrorInfo();
            libraryLog::_getInstance()->error('InsertSQL err', $this->lastError);
            $this->benchmark->End($bid);
            return false;
        }
        else
            $this->lastError = null;
        $this->lastInsertOid = $this->pdo->lastInsertId();
        $oid = $this->lastInsertOid;

        if (PDODRIVER == 'mysql')
            $this->lastInsertId = $oid;
        else {
           if($this -> debugstatus)  $this->log->warn("LastinsertDI", $this->lastInsertOid);
            $id = $this->getArray("SELECT id from $table where oid='$oid'");
            $this->lastInsertId = $id[0]['id'];
        }
        $this->benchmark->End($bid);
        return true;
    }

    private function merge_as_array($odp) {
        $arr = array();
        if ($odp === false)
            return false;
        if (empty($odp))
            return '';
        while ($row = $odp->fetch()) {
            $this->rowsCount++;
            $arr[] = $row;
        }
        return $arr;
    }

    /**
     * Pobierz typ kolumny dla bind
     * @param string $column
     * @param array $all
     */
    private function getTypeOf($column, $all) {

        $parsed = array();
        $sha1 = sha1(serialize($all));
        if (empty($parsed[$sha1])) {
            foreach ($all as $value) {
                if (preg_match('/^int/i', $value['data_type'])) {
                    //$this->log->log('INTEGER', $value);
                    $type = PDO::PARAM_INT;
                } else if (preg_match('/^boolean/i', $value['data_type'])) {
                    //$this->log->log('BOOLEAN', $value);
                    $type = PDO::PARAM_BOOL;
                } else {
                    //$this->log->log('STRING', $value);
                    $type = PDO::PARAM_STR;
                }
                $parsed[$sha1][$value['column_name']] = $type;
            }
        }

        return $parsed[$sha1][$column];
    }

    public function updateSQL($array, $table, $condition = '') {
        $bid = $this->benchmark->Start("updateSQL INTO TABLE " . $table);
        $q = "select column_name,data_type,character_maximum_length from INFORMATION_SCHEMA.COLUMNS where table_name = '$table'";
        $arr = $this->getArray($q, false);
        $cols = array();
        foreach ($arr as $value) {
            $cols[] = $value['column_name'];
        }
        $binds = array();
        foreach ($array as $key => $value) {

            if (!in_array($key, $cols))
                continue;

            $keys[] = $key;
            $dane['bind'] = ":$key";
            $dane['value'] = $value;
            $dane['type'] = $this->getTypeOf($key, $arr);
            $binds[] = $dane;
            $keybind[] = "$key = :$key";
        }

        /* $keys_ = implode("`, `", $keys);
          $keys__ = implode(", :", $keys);
         */
        libraryLog::_getInstance()->info('keybind', $dane);
        $keybind = implode(", ", $keybind);

        $q = $condition=='' ? "UPDATE $table  SET $keybind" : "UPDATE $table  SET $keybind WHERE $condition";
        $this->benchmark->Modify($bid, $q);
        $statment = $this->pdo->prepare($q);
        foreach ($binds as $value) {
            $statment->bindValue($value['bind'], $value['value'], $value['type']);
        }


        $stat = $statment->execute();

        if (!$stat) {
            $err = $statment->ErrorInfo();
            $this->lastError = $err;
            libraryLog::_getInstance()->error($q, $err[2]);
        }
        $this->benchmark->End($bid);

        return $stat;
    }

    /**
     * pobierz ostatni insert id 
     */
    public function lastId() {

        return $this->lastInsertId;
    }

    public function errorMSG() {
        return $this->lastError;
    }

    public function Execute($q) {
        $bid = $this->benchmark->Start($q);

        $cnt = $this->pdo->exec($q);
        if ($cnt === false)
            $this->log->error($q, $this->pdo->errorInfo());
        $this->benchmark->Modify($bid, $q . " ($cnt)");
        $this->benchmark->End($bid);
    }

    
    /**
     *Switch if log debug infos from Database
     * @param boolean $status
     * @return boolean active status
     */
    public function debug($status = null){
        if($status === null) return $this -> debugstatus;
        else $this -> debugstatus = $status;
        
        return $this -> debugstatus;
    }
}

?>