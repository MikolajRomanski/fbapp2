<?php

class Benchmark {

    private $count = 0;
    private $name = array();
    private $closed = array();
    private $elements = array();
    private $group = array();
    private $status = false;
    private static $_init = false;

    public function __construct() {
        chdir(WORKING_DIR); 
    }

    /**
     * Pobierz instancję obiektu Benchmark
     * @author Mikołaj Romański <mikolaj@doromi.net>
     * @return Benchmark
     */
    public static function _getInstance() {
        if (!self::$_init) {
            //parent::_getInstance();
            self::$_init = new Benchmark();
            self::$_init->Start('Główny skrypt', false);
        }
        return self::$_init;
    }
    
    public function disable_benchmark( $status = false){
        $this -> status = $status;
    }

    /**
     * Rozpocznili wyliczanie benchmarku dla elementu $name
     * @author Mikołaj Romański <mikolaj@doromi.net>
     * @param string $name
     * @@param boolean $changecount nie zmieniać, jeśli naprawdę nie trzeba!
     * @return int id
     */
    public function Start($name, $changecount = true, $group = false) {
        if($this -> status) return;
        if ($changecount)
            $this->count++;
        $this->name[$this->count] = $name;
        $this->elements[$this->count]['s'] = time();
        $this->elements[$this->count]['mytime'] = $this->microtime_float();

        $time = microtime();
        $time_ = explode(' ', $time);

        $this->elements[$this->count]['m'] = ceil(100 * $time_[0]);
        if ($group !== false)
            $this->group[$group][$this->count]['m'] = ceil($time_[0] * 100);
        //libraryLog::_getInstance() -> log('benchmark start '.$name, $this -> count);
        return $this->count;
    }

    /**
     * Zakończ wyliczanie benchamrku dla elementu $naem
     * @author Mikołaj Romański <mikolaj@doromi.net>
     * @param int $id
     */
    public function End($id) {
        if($this -> status) return;
        //libraryLog::_getInstance() -> log('benchmark END', $id);
        //$this -> elements[ $id ] = time() + microtime( true ) - $this -> elements[ $id ];
        $this->closed[$id]['s'] = time();
        $time = microtime();
        $time_ = explode(' ', $time);
        $this->closed[$id]['m'] = ceil(100 * $time_[0]);
        $this->closed[$id]['mytime'] = $this->microtime_float();
    }

    /**
     * Wrzuć do loga pełny raport benchmark
     * @@author Mikołaj Romański <mikolaj@doromi.net>
     */
    public function Raport() {

        $this->End(0);
        $bench[0] = array('Operacja', 'Realtime', 'Procent');
        foreach ($this->elements as $key => $value) {
            if (empty($this->closed))
                $this->log()->warn('Problem zamknięcia benchmarka', array('nazwa' => $this->name[$key], 'start' => $value));
            else {


                $time = $this->closed[0]['mytime'] - $this->elements[0]['mytime'];
                $mytime = $this->closed[$key]['mytime'] - $this->elements[$key]['mytime'];
                if ($time > 0)
                    $proc = round((100 * $mytime / $time), 3) . '%';
                else
                    $proc = 'unknown';

                $dif['s'] = $this->closed[$key]['s'] - $value['s'];
                $dif['m'] = $this->closed[$key]['m'] - $value['m'];
                $dif['total'] = $this->closed[$key]['mytime'] - $value['mytime'];

                $bench[] = array($this->name[$key], round($dif['total'], 5), $proc);
            }
        }

        $log = libraryLog::_getInstance();
        $log->group_start('Benchmark', '#0C495F');
        $log->info('Ogólny czas wykonania skryptu', round($time, 5) . 's');
        $log->info('Pamięć użyta', Files::getfilesize(memory_get_peak_usage()) . ', real: ' . Files::getfilesize(memory_get_peak_usage(true)));
        $log->table('Czasy ogółem', $bench);
        $log->group_end();
    }

    private function microtime_float() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }
    
    /**
     *Zmień etykietę benchmarku
     * @param type $bid
     * @param type $value 
     */
    public function Modify($bid, $value){
        $this->name[$bid] = $value;
    }

}