<?php

class objCSS {

    private $files = array();
    private $csscontent = array();

    public function __construct() {
        chdir(WORKING_DIR); 
        //require_once '3rdparty/cssmin.php';
    }

    public static function getInstance() {
        static $_inst = null;
        if ($_inst == null)
            $_inst = new objCSS();
        return $_inst;
    }

    public function add($file, $path = '') {
        $benchmark = Benchmark::_getInstance();
        $css_ = $benchmark->Start('Add CSS ' . $file);
        //$css = objCSS::getInstance();
        $this ->addCSSFile($file, $path);
        $benchmark->End($css_);
    }

    public function generate() {
        $benchmark = Benchmark::_getInstance();
        $b = $benchmark ->Start('Generuj CSS');
        libraryLog::_getInstance() -> info('files', $this -> files);
        $nj = array();
        if (CSS_COMPRESS) {
            foreach ($this->files as $key => $value) {
                $filetime = filemtime($value);
                $cssfilename = $key . '_' . $filetime . '_single_compress.css';
                $filename = CSSDIR . $key . '_' . $filetime . '_single_compress.css';

                if (!file_exists($filename)) {
                    $content[$key] = cssmin::minify(file_get_contents($value));
                    $plik = fopen($filename, "w");
                    fputs($plik, $content[$key]);
                    fclose($plik);
                }

                $nj[$key] = $cssfilename;
            }
            $this->files = $nj;
        } else {
            foreach ($this->files as $key => $value) {
                $filetime = filemtime($value);
                $cssfilename = $key . '_' . $filetime . '_single_normal.css';
                $filename = CSSDIR . $key . '_' . $filetime . '_single_normal.css';
                if (!file_exists($filename)) {
                    $content[$key] = file_get_contents($value);
                    $plik = fopen($filename, "w");
                    fputs($plik, $content[$key]);
                    fclose($plik);
                }
                $nj[$key] = $cssfilename;
            }
        }
        
        $this -> files = $nj;
        
        if (CSS_ONE_FILE) {
            if (isset($nj))
                $nj = array();

            foreach ($this->files as $key => $value) {
                $content[$key] = file_get_contents(CSSDIR.$value);
            }
            $content = implode("\n", $content);
            $cssfilename = sha1($content) . '_onefile_.css';
            $sha1 = CSSDIR . sha1($content) . '_onefile_.css';
            if (!file_exists($sha1)) {
                $plik = fopen($sha1, "w");
                fputs($plik, $content);
                fclose($plik);
            }
            else
                $content = file_get_contents($sha1);
            unset($nj);
            $nj[sha1($sha1)] = $cssfilename;
            $this->files = $nj;
            //return $content;
        }

        

        $benchmark ->End($b);

        return $this->files;
    }

    private function addCSSFile($file, $path) {
        
        $filename = empty($path) ? $path . 'css/' . Translate::make_filename($file) : $path.Translate::make_filename($file);
        if (file_exists($filename)) {
            $this->files[Translate::make_url($filename) . sha1($filename)] = $filename;
            //libraryLog::_getInstance() -> warn('Dodaję css', $filename);	
        } else {
            libraryLog::_getInstance()->error('CSS - nie istnieje', $filename);
        }
    }

}

?>