<?php

Class UseSmarty {

    private $admin = null;
    private $clients = null;

    public function __construct() {
        
    }

    /**
     * Instancja obiektu Smarty
     * @author Mikołaj Romański
     * @return Smarty
     */
    public static function getInstance() {
        chdir(WORKING_DIR); 
        static $admin = null;
        if ($admin == null) {
            require_once '3rdparty/smarty/libs/Smarty.class.php';
            $admin = new Smarty();
            $admin->template_dir = TEMPLATEDIR;
            $admin->compile_dir = TEMPLATESC;
            $admin->debugging = DEBUGSMARTY;
        }
        return $admin;
    }

    public static function single_action() {


        require_once '3rdparty/smarty/libs/Smarty.class.php';
        $smarty = new Smarty();
        //$smarty -> template_dir = 'templates';
        $smarty->compile_dir = TEMPLATESC;
        $smarty->debugging = DEBUGSMARTY;

        return $smarty;
    }

}

?>
