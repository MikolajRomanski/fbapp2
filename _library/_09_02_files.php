<?php

class Files {

    public static function get_extension($type) {
        switch ($type) {
            case 'image/png':
                return '.png';
                break;
            case 'image/jpeg':
                return '.jpg';
                break;
            case 'image/pjpeg':
                return '.jpg';
                break;
            case 'image/gif':
                return '.gif';
                break;

            default:
                return '.unknow';
                break;
        }
    }

    public static function is_allowed_ext_gallery($ext) {
        if (in_array($ext, array('.jpg', '.png', '.jpeg')))
            return true;
        else
            return false;
    }

    public static function get_type_by_extension($ext) {
        switch ($ext) {
            case '.png':
                return 'image/png';
                break;
            case '.jpg':
                return 'image/jpeg';
                break;
            case '.jpg':
                return 'image/pjpeg';
                break;
            case '.gif':
                return 'image/gif';
                break;
            default:
                return '.unknow';
                break;
        }
    }

    public static function get_extension_by_filename($name) {

        $dot = explode('.', $name);
        $count = count($dot);
        if ($count == 1) {
            libraryLog::_getinstance()->warn('FILENAME EXTENSION', $name . " -> .file");
            return '.file';
        } else {

            return '.' . $dot[$count - 1];
        }
    }

    public static function getfilesize($bytes) {

        $jednostki = array('B', 'KB', 'MB', 'GB', 'TB');
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($jednostki) - 1);
        $p = pow(1024, $pow);
        if ($p != 0)
            $bytes /= $p;
        return round($bytes, 2) . ' ' . $jednostki[$pow];
        return $bytes;
    }

    public static function clear_temporary() {
        $donatdelete = array('.jpg', '.jpeg', '.png', '.JPG', '.JPEG', '.PNG');
        if (!is_dir(TMPDIR)) {
            //libraryLog::_getInstance()->error('Nie czyszczę TMPDIR - brak katalogu', TMPDIR);
            return false;
        }
        $deletetime = time() - 3600 * 24;
        $dir = opendir(TMPDIR);
        while ($plik = readdir($dir)) {
            if (is_file(TMPDIR . $plik)) {
                if (fileatime(TMPDIR . $plik) < $deletetime) {
                    //libraryLog::_getInstance()->info('Czas dostępu ('.$plik.'): ', date('Y-m-d H:i:s', fileatime((TMPDIR . $plik))) . ", utworzono: " . date('Y-m-d H:i:s', filemtime((TMPDIR . $plik))));
                    $ext = self::get_extension_by_filename($plik);
                    
                    if (!in_array($ext, $donatdelete)) {
                        if (!unlink(TMPDIR . $plik))
                            libraryLog::_getInstance()->error("Błąd usuwania", TMPDIR . $plik);
                    }
                }
                else {
                    //libraryLog::_getInstance() -> setlog(TMPDIR.$plik, libraryFiles::getfilesize(filesize(TMPDIR.$plik))." / dostęp: ".date('Y-m-d H:i:s', fileatime((TMPDIR.$plik) ) )." utworzono: ".date('Y-m-d H:i:s', filemtime(TMPDIR.$plik) ) );
                }
            }
        }

        $dir = opendir(ERRORDIR);
        $deletetime = time() - 3600 * 24 * 2;
        while ($plik = readdir($dir)) {
            if (is_file(ERRORDIR . $plik)) {
                if (fileatime(ERRORDIR . $plik) < $deletetime) {
                    libraryLog::_getInstance()->info('Czas dostępu ('.$plik.'): ', date('Y-m-d H:i:s', fileatime((ERRORDIR . $plik))) . ", utworzono: " . date('Y-m-d H:i:s', filemtime((ERRORDIR . $plik))));
                    if (!unlink(ERRORDIR . $plik))
                        libraryLog::_getInstance()->error("Błąd usuwania", ERRORDIR . $plik);
                }
                else {
                    //libraryLog::_getInstance() -> setlog(ERRORDIR.$plik, libraryFiles::getfilesize(filesize(ERRORDIR.$plik))." / dostęp: ".date('Y-m-d H:i:s', fileatime((ERRORDIR.$plik) ) )." utworzono: ".date('Y-m-d H:i:s', filemtime(ERRORDIR.$plik) ) );
                }
            }
        }


        $dir = opendir(SESSDIR);
        $deletetime = time() - 3600 * 24;
        while ($plik = readdir($dir)) {
            if (is_file(SESSDIR . $plik)) {
                if (fileatime(SESSDIR . $plik) < $deletetime) {
                    //libraryLog::_getInstance()->info('Czas dostępu ('.$plik.'): ', date('Y-m-d H:i:s', fileatime((SESSDIR . $plik))) . ", utworzono: " . date('Y-m-d H:i:s', filemtime(SESSDIR . $plik)));
                    if (!unlink(SESSDIR . $plik))
                        libraryLog::_getInstance()->error("Błąd usuwania", SESSDIR . $plik);
                }
                else {
                    // libraryLog::_getInstance() -> setlog(TMPDIR.'SESS/'.$plik, libraryFiles::getfilesize(filesize(TMPDIR.'SESS/'.$plik))." / dostęp: ".date('Y-m-d H:i:s', fileatime((TMPDIR.'SESS/'.$plik) ) )." utworzono: ".date('Y-m-d H:i:s', filemtime(TMPDIR.'SESS/'.$plik) ) );
                }
            }
        }
        
        $dirname = JSDIR;
        $dir = opendir($dirname);
        $deletetime = time() - 3600 * 24;
        libraryLog::_getInstance()->info('in jsdir', $dirname);
        while ($plik = readdir($dir)) {
            if (is_file($dirname . $plik)) {
                libraryLog::_getInstance()->log('Czas dostępu ('.$dirname.$plik.'): ', date('Y-m-d H:i:s', fileatime((TMPDIR . $plik))) . ", utworzono: " . date('Y-m-d H:i:s', filemtime((TMPDIR . $plik))));
                if (fileatime($dirname . $plik) < $deletetime) {
                    libraryLog::_getInstance()->log('Czas dostępu ('.$plik.'): ', date('Y-m-d H:i:s', fileatime(($dirname . $plik))) . ", utworzono: " . date('Y-m-d H:i:s', filemtime($dirname . $plik)));
                    if (!unlink($dirname . $plik))
                        libraryLog::_getInstance()->error("Błąd usuwania", $dirname . $plik);
                }
                else {
                    // libraryLog::_getInstance() -> setlog(TMPDIR.'SESS/'.$plik, libraryFiles::getfilesize(filesize(TMPDIR.'SESS/'.$plik))." / dostęp: ".date('Y-m-d H:i:s', fileatime((TMPDIR.'SESS/'.$plik) ) )." utworzono: ".date('Y-m-d H:i:s', filemtime(TMPDIR.'SESS/'.$plik) ) );
                }
            }
        }
        
        $dirname = CSSDIR;
        $dir = opendir($dirname);
        $deletetime = time() - 3600 * 24;
        while ($plik = readdir($dir)) {
            if (is_file($dirname . $plik)) {
                if (fileatime($dirname . $plik) < $deletetime) {
                    libraryLog::_getInstance()->info('Czas dostępu ('.$plik.'): ', date('Y-m-d H:i:s', fileatime(($dirname . $plik))) . ", utworzono: " . date('Y-m-d H:i:s', filemtime($dirname . $plik)));
                    if (!unlink($dirname . $plik))
                        libraryLog::_getInstance()->error("Błąd usuwania", $dirname . $plik);
                }
                else {
                    // libraryLog::_getInstance() -> setlog(TMPDIR.'SESS/'.$plik, libraryFiles::getfilesize(filesize(TMPDIR.'SESS/'.$plik))." / dostęp: ".date('Y-m-d H:i:s', fileatime((TMPDIR.'SESS/'.$plik) ) )." utworzono: ".date('Y-m-d H:i:s', filemtime(TMPDIR.'SESS/'.$plik) ) );
                }
            }
        }
        
        
        
        
    }

    public static function make_stats() {
        return false;
    }

    /**
     * Pobierz mime plik z nazwy
     */
    public static function mime_type($file) {
        $mime_types = array(
            "323" => "text/h323",
            "acx" => "application/internet-property-stream",
            "ai" => "application/postscript",
            "aif" => "audio/x-aiff",
            "aifc" => "audio/x-aiff",
            "aiff" => "audio/x-aiff",
            "asf" => "video/x-ms-asf",
            "asr" => "video/x-ms-asf",
            "asx" => "video/x-ms-asf",
            "au" => "audio/basic",
            "avi" => "video/x-msvideo",
            "axs" => "application/olescript",
            "bas" => "text/plain",
            "bcpio" => "application/x-bcpio",
            "bin" => "application/octet-stream",
            "bmp" => "image/bmp",
            "c" => "text/plain",
            "cat" => "application/vnd.ms-pkiseccat",
            "cdf" => "application/x-cdf",
            "cer" => "application/x-x509-ca-cert",
            "class" => "application/octet-stream",
            "clp" => "application/x-msclip",
            "cmx" => "image/x-cmx",
            "cod" => "image/cis-cod",
            "cpio" => "application/x-cpio",
            "crd" => "application/x-mscardfile",
            "crl" => "application/pkix-crl",
            "crt" => "application/x-x509-ca-cert",
            "csh" => "application/x-csh",
            "css" => "text/css",
            "dcr" => "application/x-director",
            "der" => "application/x-x509-ca-cert",
            "dir" => "application/x-director",
            "dll" => "application/x-msdownload",
            "dms" => "application/octet-stream",
            "doc" => "application/msword",
            "dot" => "application/msword",
            "dvi" => "application/x-dvi",
            "dxr" => "application/x-director",
            "eps" => "application/postscript",
            "etx" => "text/x-setext",
            "evy" => "application/envoy",
            "exe" => "application/octet-stream",
            "fif" => "application/fractals",
            "flr" => "x-world/x-vrml",
            "gif" => "image/gif",
            "gtar" => "application/x-gtar",
            "gz" => "application/x-gzip",
            "h" => "text/plain",
            "hdf" => "application/x-hdf",
            "hlp" => "application/winhlp",
            "hqx" => "application/mac-binhex40",
            "hta" => "application/hta",
            "htc" => "text/x-component",
            "htm" => "text/html",
            "html" => "text/html",
            "htt" => "text/webviewhtml",
            "ico" => "image/x-icon",
            "ief" => "image/ief",
            "iii" => "application/x-iphone",
            "ins" => "application/x-internet-signup",
            "isp" => "application/x-internet-signup",
            "jfif" => "image/pipeg",
            "jpe" => "image/jpeg",
            "jpeg" => "image/jpeg",
            "jpg" => "image/jpeg",
            "js" => "application/x-javascript",
            "latex" => "application/x-latex",
            "lha" => "application/octet-stream",
            "lsf" => "video/x-la-asf",
            "lsx" => "video/x-la-asf",
            "lzh" => "application/octet-stream",
            "m13" => "application/x-msmediaview",
            "m14" => "application/x-msmediaview",
            "m3u" => "audio/x-mpegurl",
            "man" => "application/x-troff-man",
            "mdb" => "application/x-msaccess",
            "me" => "application/x-troff-me",
            "mht" => "message/rfc822",
            "mhtml" => "message/rfc822",
            "mid" => "audio/mid",
            "mny" => "application/x-msmoney",
            "mov" => "video/quicktime",
            "movie" => "video/x-sgi-movie",
            "mp2" => "video/mpeg",
            "mp3" => "audio/mpeg",
            "mpa" => "video/mpeg",
            "mpe" => "video/mpeg",
            "mpeg" => "video/mpeg",
            "mpg" => "video/mpeg",
            "mpp" => "application/vnd.ms-project",
            "mpv2" => "video/mpeg",
            "ms" => "application/x-troff-ms",
            "mvb" => "application/x-msmediaview",
            "nws" => "message/rfc822",
            "oda" => "application/oda",
            "p10" => "application/pkcs10",
            "p12" => "application/x-pkcs12",
            "p7b" => "application/x-pkcs7-certificates",
            "p7c" => "application/x-pkcs7-mime",
            "p7m" => "application/x-pkcs7-mime",
            "p7r" => "application/x-pkcs7-certreqresp",
            "p7s" => "application/x-pkcs7-signature",
            "pbm" => "image/x-portable-bitmap",
            "pdf" => "application/pdf",
            "pfx" => "application/x-pkcs12",
            "pgm" => "image/x-portable-graymap",
            "pko" => "application/ynd.ms-pkipko",
            "pma" => "application/x-perfmon",
            "pmc" => "application/x-perfmon",
            "pml" => "application/x-perfmon",
            "pmr" => "application/x-perfmon",
            "pmw" => "application/x-perfmon",
            "pnm" => "image/x-portable-anymap",
            "pot" => "application/vnd.ms-powerpoint",
            "ppm" => "image/x-portable-pixmap",
            "pps" => "application/vnd.ms-powerpoint",
            "ppt" => "application/vnd.ms-powerpoint",
            "prf" => "application/pics-rules",
            "ps" => "application/postscript",
            "pub" => "application/x-mspublisher",
            "qt" => "video/quicktime",
            "ra" => "audio/x-pn-realaudio",
            "ram" => "audio/x-pn-realaudio",
            "ras" => "image/x-cmu-raster",
            "rgb" => "image/x-rgb",
            "rmi" => "audio/mid",
            "roff" => "application/x-troff",
            "rtf" => "application/rtf",
            "rtx" => "text/richtext",
            "scd" => "application/x-msschedule",
            "sct" => "text/scriptlet",
            "setpay" => "application/set-payment-initiation",
            "setreg" => "application/set-registration-initiation",
            "sh" => "application/x-sh",
            "shar" => "application/x-shar",
            "sit" => "application/x-stuffit",
            "snd" => "audio/basic",
            "spc" => "application/x-pkcs7-certificates",
            "spl" => "application/futuresplash",
            "src" => "application/x-wais-source",
            "sst" => "application/vnd.ms-pkicertstore",
            "stl" => "application/vnd.ms-pkistl",
            "stm" => "text/html",
            "svg" => "image/svg+xml",
            "sv4cpio" => "application/x-sv4cpio",
            "sv4crc" => "application/x-sv4crc",
            "t" => "application/x-troff",
            "tar" => "application/x-tar",
            "tcl" => "application/x-tcl",
            "tex" => "application/x-tex",
            "texi" => "application/x-texinfo",
            "texinfo" => "application/x-texinfo",
            "tgz" => "application/x-compressed",
            "tif" => "image/tiff",
            "tiff" => "image/tiff",
            "tr" => "application/x-troff",
            "trm" => "application/x-msterminal",
            "tsv" => "text/tab-separated-values",
            "txt" => "text/plain",
            "uls" => "text/iuls",
            "ustar" => "application/x-ustar",
            "vcf" => "text/x-vcard",
            "vrml" => "x-world/x-vrml",
            "wav" => "audio/x-wav",
            "wcm" => "application/vnd.ms-works",
            "wdb" => "application/vnd.ms-works",
            "wks" => "application/vnd.ms-works",
            "wmf" => "application/x-msmetafile",
            "wps" => "application/vnd.ms-works",
            "wri" => "application/x-mswrite",
            "wrl" => "x-world/x-vrml",
            "wrz" => "x-world/x-vrml",
            "xaf" => "x-world/x-vrml",
            "xbm" => "image/x-xbitmap",
            "xla" => "application/vnd.ms-excel",
            "xlc" => "application/vnd.ms-excel",
            "xlm" => "application/vnd.ms-excel",
            "xls" => "application/vnd.ms-excel",
            "xlt" => "application/vnd.ms-excel",
            "xlw" => "application/vnd.ms-excel",
            "xof" => "x-world/x-vrml",
            "xpm" => "image/x-xpixmap",
            "xwd" => "image/x-xwindowdump",
            "z" => "application/x-compress",
            "zip" => "application/zip",
            'ez' => 'application/andrew-inset',
            'hqx' => 'application/mac-binhex40',
            'cpt' => 'application/mac-compactpro',
            'doc' => 'application/msword',
            'bin' => 'application/octet-stream',
            'dms' => 'application/octet-stream',
            'lha' => 'application/octet-stream',
            'lzh' => 'application/octet-stream',
            'exe' => 'application/octet-stream',
            'class' => 'application/octet-stream',
            'so' => 'application/octet-stream',
            'dll' => 'application/octet-stream',
            'oda' => 'application/oda',
            'pdf' => 'application/pdf',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            'smi' => 'application/smil',
            'smil' => 'application/smil',
            'mif' => 'application/vnd.mif',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'wbxml' => 'application/vnd.wap.wbxml',
            'wmlc' => 'application/vnd.wap.wmlc',
            'wmlsc' => 'application/vnd.wap.wmlscriptc',
            'bcpio' => 'application/x-bcpio',
            'vcd' => 'application/x-cdlink',
            'pgn' => 'application/x-chess-pgn',
            'cpio' => 'application/x-cpio',
            'csh' => 'application/x-csh',
            'dcr' => 'application/x-director',
            'dir' => 'application/x-director',
            'dxr' => 'application/x-director',
            'dvi' => 'application/x-dvi',
            'spl' => 'application/x-futuresplash',
            'gtar' => 'application/x-gtar',
            'hdf' => 'application/x-hdf',
            'js' => 'application/x-javascript',
            'skp' => 'application/x-koan',
            'skd' => 'application/x-koan',
            'skt' => 'application/x-koan',
            'skm' => 'application/x-koan',
            'latex' => 'application/x-latex',
            'nc' => 'application/x-netcdf',
            'cdf' => 'application/x-netcdf',
            'sh' => 'application/x-sh',
            'shar' => 'application/x-shar',
            'swf' => 'application/x-shockwave-flash',
            'sit' => 'application/x-stuffit',
            'sv4cpio' => 'application/x-sv4cpio',
            'sv4crc' => 'application/x-sv4crc',
            'tar' => 'application/x-tar',
            'tcl' => 'application/x-tcl',
            'tex' => 'application/x-tex',
            'texinfo' => 'application/x-texinfo',
            'texi' => 'application/x-texinfo',
            't' => 'application/x-troff',
            'tr' => 'application/x-troff',
            'roff' => 'application/x-troff',
            'man' => 'application/x-troff-man',
            'me' => 'application/x-troff-me',
            'ms' => 'application/x-troff-ms',
            'ustar' => 'application/x-ustar',
            'src' => 'application/x-wais-source',
            'xhtml' => 'application/xhtml+xml',
            'xht' => 'application/xhtml+xml',
            'zip' => 'application/zip',
            'au' => 'audio/basic',
            'snd' => 'audio/basic',
            'mid' => 'audio/midi',
            'midi' => 'audio/midi',
            'kar' => 'audio/midi',
            'mpga' => 'audio/mpeg',
            'mp2' => 'audio/mpeg',
            'mp3' => 'audio/mpeg',
            'aif' => 'audio/x-aiff',
            'aiff' => 'audio/x-aiff',
            'aifc' => 'audio/x-aiff',
            'm3u' => 'audio/x-mpegurl',
            'ram' => 'audio/x-pn-realaudio',
            'rm' => 'audio/x-pn-realaudio',
            'rpm' => 'audio/x-pn-realaudio-plugin',
            'ra' => 'audio/x-realaudio',
            'wav' => 'audio/x-wav',
            'pdb' => 'chemical/x-pdb',
            'xyz' => 'chemical/x-xyz',
            'bmp' => 'image/bmp',
            'gif' => 'image/gif',
            'ief' => 'image/ief',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'jpe' => 'image/jpeg',
            'png' => 'image/png',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'djvu' => 'image/vnd.djvu',
            'djv' => 'image/vnd.djvu',
            'wbmp' => 'image/vnd.wap.wbmp',
            'ras' => 'image/x-cmu-raster',
            'pnm' => 'image/x-portable-anymap',
            'pbm' => 'image/x-portable-bitmap',
            'pgm' => 'image/x-portable-graymap',
            'ppm' => 'image/x-portable-pixmap',
            'rgb' => 'image/x-rgb',
            'xbm' => 'image/x-xbitmap',
            'xpm' => 'image/x-xpixmap',
            'xwd' => 'image/x-xwindowdump',
            'igs' => 'model/iges',
            'iges' => 'model/iges',
            'msh' => 'model/mesh',
            'mesh' => 'model/mesh',
            'silo' => 'model/mesh',
            'wrl' => 'model/vrml',
            'vrml' => 'model/vrml',
            'css' => 'text/css',
            'html' => 'text/html',
            'htm' => 'text/html',
            'asc' => 'text/plain',
            'txt' => 'text/plain',
            'rtx' => 'text/richtext',
            'rtf' => 'text/rtf',
            'sgml' => 'text/sgml',
            'sgm' => 'text/sgml',
            'tsv' => 'text/tab-separated-values',
            'wml' => 'text/vnd.wap.wml',
            'wmls' => 'text/vnd.wap.wmlscript',
            'etx' => 'text/x-setext',
            'xsl' => 'text/xml',
            'xml' => 'text/xml',
            'mpeg' => 'video/mpeg',
            'mpg' => 'video/mpeg',
            'mpe' => 'video/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            'mxu' => 'video/vnd.mpegurl',
            'avi' => 'video/x-msvideo',
            'movie' => 'video/x-sgi-movie',
            'ice' => 'x-conference/x-cooltalk'
        );

        $path_info = pathinfo($file);
        $ext = $path_info['extension'];
        return isset($mime_types[$ext]) ? $mime_types[$ext] : '';
    }

    public static function clear_cache($dont = array()) {
        $dir = opendir(CACHEDIR);
        $deleted = array();
        while ($plik = readdir($dir)) {
            if (!is_dir($plik) && $plik != '.htaccess') {
                $deleted[] = CACHEDIR . '/' . $plik;
                unlink(CACHEDIR . '/' . $plik);
            }
        }

        libraryLog::_getInstance()->setwarning('CLEAR CACHE', $deleted);

        
    }

    public static function convertBytes($value) {
        if (is_numeric($value)) {
            return $value;
        } else {
            $value_length = strlen($value);
            $qty = substr($value, 0, $value_length - 1);
            $unit = strtolower(substr($value, $value_length - 1));
            switch ($unit) {
                case 'k':
                    $qty *= 1024;
                    break;
                case 'm':
                    $qty *= 1048576;
                    break;
                case 'g':
                    $qty *= 1073741824;
                    break;
            }
            return $qty;
        }
    }

    public function make_backup() {
        if (!is_dir(TMPDIR . '_backups')) {
            mkdir(TMPDIR . '_backups');
        }
        $dir = TMPDIR . '_backups/';
        if (!file_exists($dir . 'index.html')) {
            touch($dir . 'index.html');
            touch($dir . 'index.htm');
        }
        $data = date('Y-m-d');
        $file = $dir . sha1($data) . $data . '.php'; //sprytne zabezpieczenie... wyrzuci błąd php, nie pozwoli pobrać. Niestety htaccess odpada - serwer nie obsługuje.

        if (file_exists($file))
            return true;
        libraryLog::_getInstance()->setwarning('BACKUP', $file);
        require_once("3dpart/iam_backup.php");


        $dirs = opendir($dir);
        while ($plik = readdir($dirs)) {
            if ($plik != 'index.html' && $plik != 'index.htm' && !is_dir($dir . $plik)) {
                $mktime = filemtime($dir . $plik);
                $diff = time() - $mktime;
                if ($diff > 1728000)
                    unlink($dir . $plik);
            }
        }

        $name = $file;
        $backup = new iam_backup(DB_HOST, DB_NAME, DB_USR, DB_PASS, false, false, true, $name);
        $backup->perform_backup();
    }

}

?>
