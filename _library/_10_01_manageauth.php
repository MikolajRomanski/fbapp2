<?php

class Auth {

    /**
     *
     * @var libraryLog
     */
    private $log;

    /**
     *
     * @var Database
     */
    private $db;

    public function __construct() {
        $this->log = libraryLog::_getInstance();
        $this->db = Database::_getInstance();
    }

    public static function _getInstance() {
        static $_inst = null;
        if ($_inst === null) {
            $_inst = new Auth();
        }

        return $_inst;
    }

    /**
     * Check if user is logged in
     * @return type 
     */
    public function is_auth() {
        
        if(empty($_SESSION['panelauthuser'])) return false;
        
        return $this ->checkauth();
        
        return false;
    }

    /**
     * Authorize user by login and password
     * @param type $login
     * @param type $password 
     */
    public function authorize($login, $password) {
        $login = trim($login);
        $password = trim($password);
        if ($login == '' || $password == '')
            return false;

        $login = $this->db->getArray("SELECT id FROM _admin WHERE email='$login' AND SHA1(CONCAT('$password', `salt`)) = `password`");
        if (empty($login))
            return false;

        $dane['lastip'] = $_SERVER['REMOTE_ADDR'];
        $dane['lastlogin'] = time();
        $dane['sessid'] = session_id();
        $this->db->updateSQL($dane, _admin, "id='" . $login[0]['id'] . "'");
        $_SESSION['panelauthuser'] = $login[0]['id'];
        
        return true;
    }

    public function logout() {
        if (!empty($_SESSION['panelauthuser'])) {
            $dane['sessid'] = '';
            $this->db->updateSQL($dane, '_admin', "id='" . $_SESSION['panelauthuser'] . "'");
            unset($_SESSION['panelauthuser']);
        }
    }
    
    private function checkauth(){
        static $auth = '';
        if($auth !='') return $auth;
        
        $info = $this -> db ->getArray("SELECT COUNT(1) AS ilosc FROM _admin WHERE id='" . $_SESSION['panelauthuser'] . "' AND sessid='".session_id()."'");
        return $info[0]['ilosc'] != 0;
        
    }

}

?>
