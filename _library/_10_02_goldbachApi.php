<?php

class goldbachApi {

    /**
     *
     * @var libraryLog
     */
    private $log;

    /**
     *
     * @var Database
     */
    private $db;

    public function __construct() {
        $this->log = libraryLog::_getInstance();
        $this->db = Database::_getInstance();
    }
    
    public static function _getInstance(){
        static $_inst = null;
        if ($_inst === null){
            $_inst = new goldbachApi();
        }
        
        return $_inst;
    }
    
    public function getFBUsers(){
        $users = $this -> db ->getArray("SELECT id, userid, first_name, last_name, email, username, FROM_UNIXTIME(created) AS created 
            FROM auth WHERE userid>0 
            ORDER BY id DESC");
        return empty($users) ? '' : $users;
    }
    

}

?>