<?php

class Fcb {

    private $config = array();
    private $scope = array();

    /**
     *
     * @var Facebook
     */
    private $facebook;

    /**
     *
     * @var Benchmark
     */
    private $benchmark = null;
    private $accessToken = '';

    public function __construct() {
        chdir(WORKING_DIR);
        $this->benchmark = Benchmark::_getInstance();
        require_once '3rdparty/facebook/facebook.php';
        $this->config['appId'] = appId;

        $this->config['secret'] = secret;
        $this->config['scope'] = FBSCOPE;
        $this->config['fileUpload'] = true; // optional
        $this->scope = FBSCOPE;

        $this->facebook = new Facebook($this->config);
        
    }

    /**
     *
     * @staticvar null $_inst
     * @return Fcb 
     */
    public static function getInstance() {
        static $_inst = null;
        if ($_inst == null) {
            $_inst = new Fcb();
        }

        return $_inst;
    }

    /**
     * Sprawdź, czy user autoryzowany
     * @return boolean 
     */
    public function is_authorized($force = false) {
        $b = $this->benchmark->Start('FB is auth');
        static $isauth = null;
        if($force == false && $isauth != null){
            libraryLog::_getInstance()->log('is auth from cache', $isauth);
            $this->benchmark->End($b);
            return $isauth;
        }
        
        
        $this->facebook = new Facebook($this->config);
        $user = $this->getUserData();
        $this->benchmark->End($b);
        libraryLog::_getInstance()->warn('fbgetuser', $user);
        $isauth = $user;
        return!empty($user);
    }

    /**
     * Pobierz url do logowania 
     */
    public function getLoginUrl() {
        libraryLog::_getInstance()->log('scope', $this->scope);
        $url = $this->facebook->getLoginUrl(array('scope' => $this->scope, 'redirect_uri' => URLABS . 'auth.php'));
        return $url;
    }

    /**
     * Pobierz wsystkie dane potrzebne do autoryzacji oauth 
     */
    public function getAuthDataByUrl() {

        $atok = $this->benchmark->Start('Facebook AccessToken');
        $ex = explode('?', $_SERVER["REQUEST_URI"]);
        parse_str($ex[1], $state_);
        $state = array();
        foreach ($state_ as $key => $value) {
            $ktrim = trim($key);
            $vtrim = trim($value);
            $state[$ktrim] = $vtrim;
        }

        try {
            $state['accesstoken'] = $this->facebook->getAccessToken();
        } catch (FacebookApiException $e) {
            $this->make_err($e);
        }
        try {
            $uid = $this->facebook->getUser();
        } catch (FacebookApiException $e) {
            $this->make_err($e);
        }
        $state['userid'] = $uid;
        libraryLog::_getInstance()->log('State', $state);
        $this->benchmark->End($atok);

        return $state;
    }

    /**
     *
     * @return Facebook
     */
    public function RawFB() {
        //$this->facebook = new Facebook($this->config);
        return $this->facebook;
    }

    public function setAccessToken($token) {
        $this->accessToken = $token;
        $this->facebook->setAccessToken($this->accessToken);
    }

    public function getUserData() {
        static $userdata = null;

        if ($userdata !== null) {
            $this->logdata($userdata);
            return $userdata;
        }

        $b = $this->benchmark->Start('FB User Data');

        try {
            $udata = $this->facebook->api('/me', 'GET');
        } catch (FacebookApiException $e) {
            $userdata = false;
            $this->make_err($e);
        }

        $this->benchmark->End($b);
        $userdata = empty($udata) ? false : $udata;
        return $userdata;
    }

    /**
     * Sprawdza, czy jest fanem profilu zdefiniowanego w FBPROFILE2LIKE 
     */
    public function is_fan($force = false) {
        $ben = $this -> benchmark ->Start('Check if fan');
        static $isfan = null;
        if ($isfan !== null && $force == false){
            libraryLog::_getInstance() -> info('fan from cache', $isfan);
            $this -> benchmark ->End($ben);
            return $isfan;
        }

        
        $userid = $this->facebook->getUser();
        try {
            $dane = $this->facebook->api($userid . '/likes');
        } catch (FacebookApiException $e) {
            $this->make_err($e);
            $dane['data'] = array();
        }

        $islike = false;
        foreach ($dane['data'] as $value) {
            if ($value['id'] == FBPROFILE2LIKE)
                $islike = true;
            if ($islike)
                break;
        }
        if (!$islike) {/*
            $file = file("ignorefan.txt");
            foreach ($file as $value) {
                $v = trim($value);
                if ($v == FBPROFILE2LIKE)
                    $islike = true;
            }*/
        }

        $this->benchmark->End($ben);

        $_SESSION['isfan'] = $islike;
        $isfan = $islike;
        return $islike;
    }

    public function make_err($e) {
        libraryLog::_getInstance()->group_start('FB CATCH', '#FF0000');
        libraryLog::_getInstance()->trace('FB_EXEPT #178');
        libraryLog::_getInstance()->error('FB', $e);
        libraryLog::_getInstance()->group_end();
    }

    /**
     * Operacje na signed_request
     * @param type $signed_request
     * @return type 
     *//*
      public function parse_signed_request($signed_request) {
      list($encoded_sig, $payload) = explode('.', $signed_request, 2);

      // decode the data
      $sig = $this -> base64_url_decode($encoded_sig);
      $data = json_decode($this -> base64_url_decode($payload), true);

      return $data;
      } */

    public function parse_signed_request($signed_request, $secret) {
        list($encoded_sig, $payload) = explode('.', $signed_request, 2);

        // decode the data
        $sig = $this->base64_url_decode($encoded_sig);
        $data = json_decode($this->base64_url_decode($payload), true);

        if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
            error_log('Unknown algorithm. Expected HMAC-SHA256');
            return null;
        }

        // check sig
        $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
        if ($sig !== $expected_sig) {
            error_log('Bad Signed JSON signature!');
            return null;
        }

        return $data;
    }

    private function base64_url_decode($input) {
        return base64_decode(strtr($input, '-_', '+/'));
    }

    public function setSessionVariables($array) {
        if (empty($array['user_id']))
            return false;

        $userid = $array['user_id'];
        $userdata = $this->getUserData();
        libraryLog::_getInstance()->info('userdata', $userdata);
    }

    /**
     * Get friends
     */
    public function getFriendslist() {

        $bench = $this->benchmark->Start('Get facebook friends');
        $friends = $this->get_friends_by_url('/me/friends/');

        $this->benchmark->End($bench);
        return $friends;
    }

    private function get_friends_by_url($url) {
        $friends = array();

        $fr = $this->RawFB()->api($url, 'GET', array('access_token' => $this->RawFB()->getAccessToken(), 'limit' => 30, 'fields' => 'id,name, first_name,last_name',));
        //libraryLog::_getInstance() -> info('friends url:'.$url, $fr);
        if (!empty($fr['data'])) {
            $friends = $fr['data'];
            if (!empty($fr['paging']['next'])) {
                $url = explode("100001475835011", $fr['paging']['next']);
                //libraryLog::_getInstance() -> log('after ex', $url);
                $friends = array_merge($friends, $this->get_friends_by_url('/me' . $url[1]));
            }
        }
        return $friends;
    }

    public function is_pagetab() {

        //userid
        $userdata = $this->getUserData();
        if (empty($_POST['signed_request']) === false)
            $signedRequest = $this->RawFB()->getSignedRequest();
        else {
            $signedRequest = $_POST['signed_request'];
        }
        if (isset($signedRequest['page']) || empty($signedRequest)) {
            $this->logdata($userdata, 'app in pagetab');
            libraryLog::_getInstance()->info('App', 'in pagetab');
            libraryLog::_getInstance()->info('signed', $signedRequest);
            return true;
        }
        $this->logdata($userdata, 'app NOT in pagetab');
        libraryLog::_getInstance()->warn('App', 'NOT in pagetab');


        return false;
    }

    /**
     * convert userdata info birthday format from 01/24/1999 into 1999-01-24
     * @param type $fbdate
     * @return type 
     */
    public function formatDateFbToSql($fbdate) {
        /*
          // facebook user_birthday format (02/19/1988)
          $date = DateTime::createFromFormat('m/d/Y', $fbdate);
          try{
          $datef = $date->format('Y-m-d');
          }
          catch(Exception $e){
          $datef = '0000-00-00';
          }

          libraryLog::_getInstance() -> log('date convert '.$fbdate,  $datef);
          // mysql format (1988-02-19)
          return $datef;
         * 
         */

        $date = explode("/", $fbdate);
        if (empty($date[2]))
            $date[2] == '0000';
        if (empty($date[1]))
            $date[1] == '00';
        if (empty($date[0]))
            $date[0] == '00';
        return $date[2] . '-' . $date[0] . '-' . $date[1];
    }

    public function logdata($userdata, $function = '') {
        /*
        $db = Database::_getInstance();
        $status = $db->debug();
        $db->debug(false);
        $dane['time'] = time();
        $dane['ip'] = $_SERVER['REMOTE_ADDR'];
        $dane['fb_data'] = serialize($userdata);
        $dane['GET'] = serialize($_GET);
        $dane['POST'] = serialize($_POST);
        $dane['SESSION'] = serialize($_SESSION);
        $dane['COOKIE'] = serialize($_COOKIE);
        $dane['browser'] = $_SERVER['HTTP_USER_AGENT'];
        $dane['url'] = $_SERVER['QUERY_STRING'];
        $dane['server'] = serialize($_SERVER);
        $dane['fb_debug'] = serialize($function);
        $db->insertSQL($dane, 'fb_log');
        $db->debug($status);
         * 
         */
    }

    public function auth_workaround($userdata) {
        /*
        if (empty($userdata['id'])) {
            $plik = fopen(TMPDIR . 'authfile-' . date('Y-m-d') . '.txt', "a");
            fputs($plik, "\nWORAROUND! ERROR!\n" . date('Y-m-d H:i:s'));
            $user_ = print_r($userdata, true);
            fputs($plik, "\n user\n" . $user_);
            fclose($plik);
            $this->logdata($userdata, array('info' => "workaround", 'desc' => 'empty userdata in workaround'));
        }*/
        $this->logdata($userdata, array("workaround"));
        $db = Database::_getInstance();
        $isindb = $this->get_informations_from_db($userdata['id']);
        if (!empty($isindb))
            return true;

        $state['accesstoken'] = $this->RawFB()->getAccessToken();
        $state['userid'] = $userdata['id'];
        $state['email'] = $userdata['email'];
        $state['first_name'] = $userdata['first_name'];
        $state['last_name'] = $userdata['last_name'];
        $state['username'] = $userdata['username'];
        
        $state['lang'] = 'pl';
        $state['ip'] = $_SERVER['REMOTE_ADDR'];
        $state['created'] = time();
        $state['mission_completed'] = 1;


        libraryLog::_getInstance() -> info('auth workaround', $state);

        $db->insertSQL($state, 'auth');
/*
        //upadte users2mission
        $cnt = $db->getArray("SELECT COUNT(1) AS cnt FROM users2missions WHERE userid='$uid' AND missionid='1'");
        if ($cnt[0]['cnt'] == 0) {
            $u2m['userid'] = $uid;
            $u2m['missionid'] = '1';
            $u2m['joined'] = time();
            $u2m['completed'] = time();
            $u2m['joined_ip'] = $_SERVER['REMOTE_ADDR'];
            $u2m['completed_ip'] = $_SERVER['REMOTE_ADDR'];
            $db->insertSQL($u2m, 'users2missions');
        }
        $plik = fopen(TMPDIR . 'authfile-' . date('Y-m-d') . '.txt', "a");
        fputs($plik, "\nMAKE WORAROUND! AUTH BUT NOT IN DB!\n" . date('Y-m-d H:i:s'));
        $user_ = print_r($userdata, true);
        fputs($plik, "\n user\n" . $user_);
        fclose($plik);
 * 
 */
    }

    private function get_informations_from_db($userid) {
        $db = Database::_getInstance();
        $data = $db->getArray("SELECT * FROM auth WHERE userid='$userid'");
        if (empty($data[0]))
            return false;
        else
            return $data[0];
    }

    public function publishphoto($arr){
        libraryLog::_getInstance() -> log('pubarr', $arr);
        try{
            $this -> facebook->api('/me/photos', 'POST', $arr);
        }
        catch (FacebookApiException $e) {
            libraryLog::_getInstance() -> error('post2wall', $e);
        }
    }
    
    
    public function post2wall($arr){
        libraryLog::_getInstance() -> log('pubarr', $arr);
        try{
            $this -> facebook->api('/me/feed', 'POST', $arr);
        }
        catch (FacebookApiException $e) {
            libraryLog::_getInstance() -> error('post2wall', $e);
        }
    }
    
}

?>
