<?php

class Twit {

    private $benchmark = null;

    public function __construct() {
        chdir(WORKING_DIR); 
        
        require_once '3rdparty/twitter/twitteroauth.php';
        $this->benchmark = Benchmark::_getInstance();
    }

    public function make_err($e) {
        libraryLog::_getInstance()->group_start('FB CATCH', '#FF0000');
        libraryLog::_getInstance()->trace('FB_EXEPT #178');
        libraryLog::_getInstance()->error('FB', $e);
        libraryLog::_getInstance()->group_end();
    }

    public function authorize() {
        
        return 'null';
        $ben = $this->benchmark->Start('twitter is_auth');
        $twitteroauth = new TwitterOAuth(TWITTER_KEY, TWITTER_SECRET);
        //https://fb.itgladiator.doromi.net/twitauth.php
        $request_token = $twitteroauth->getRequestToken(URLABS . 'twitauth.php');
        $_SESSION['oauth_token'] = $request_token['oauth_token'];  
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];  
        
        if ($twitteroauth->http_code == 200) {
            
            $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']);
            header('Location: ' . $url);
        } else {
            // It's a bad idea to kill the script, but we've got to know when there's an error.  
            die('Something wrong happened.');
        }
    }

}

?>
