<?php

class UserData {

    /**
     *
     * @var libraryLog
     */
    private $log;

    /**
     *
     * @var Database
     */
    private $db;
    private $fbuserid = null;
    private $userid = null;
    //store uploaded path to use it in object if needed
    private $imageoryginalpath = null;

    public function __construct( $userid = null) {
        $this->log = libraryLog::_getInstance();
        $this->db = Database::_getInstance();
        
        if($userid !== null){
            $this -> userid = $userid;
        }
    }

    /**
     * 
     * User resources are identified by primary key of the auth table, not facebook userid
     * This library is made to be as much compatybile with future applications
     * and social media userid
     * @author Mikołaj Romański <mikolaj@doromi.net>
     * @param facebook userid $userid
     * @return boolean 
     */
    public function set_fbuserid($userid) {
        $this->fbuserid = $userid;
        $auth = $this->db->getArray("SELECT id FROM auth WHERE userid='" . $this->fbuserid . "'");
        if (empty($auth[0]['id'])) {
            $this->log->warn('Userid not exists in auth table', $userid);
            $this->fbuserid = null;
            return false;
        }
        $this->userid = $auth[0]['id'];
        return true;
    }

    /**
     * Get element created by user by status. Null - status will be ignored
     * @param boolean status 
     */
    public function get_user_elements($status = null) {
        if (empty($this->userid)) {
            $this->log->warn('userid not defined', $this->userid);
            return false;
        }

        if ($status === null)
            $elements = $this->db->getArray("SELECT *, FROM_UNIXTIME(created) AS createdh FROM _user_elements WHERE userid = '" . $this->userid . "'");
        else
            $elements = $this->db->getArray("SELECT *, FROM_UNIXTIME(created) AS createdh FROM _user_elements WHERE userid = '" . $this->userid . "' AND status='$status'");

        $els = array();
        if (!empty($elements)) {
            foreach ($elements as $value) {
                $value['additional_data'] = unserialize($value['additional_data']);
                $els[] = $value;
            }
        }



        return empty($els) ? '' : $els;
    }

    /**
     * Create empty element, with name and description
     * @param type $name
     * @param type $description 
     */
    public function create_element($name = '', $description = '') {
        if (empty($this->userid)) {
            $this->log->warn('userid not defined', $this->userid);
            return false;
        }
        $data['created'] = time();
        $data['userid'] = $this->userid;
        $data['status'] = 0;
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        $data['name'] = $name;
        $data['description'] = $description;
        $this->db->insertSQL($data, '_user_elements');

        return $this->db->lastId();
    }

    /**
     * get pictures
     * @param type $element_id
     * @return boolean|string 
     */
    public function get_pictures_from_element($element_id, $order = 'DESC') {
        if (empty($this->userid)) {
            $this->log->warn('userid not defined', $this->userid);
            return false;
        }

        $elements = $this->db->getArray("SELECT * FROM _elements_pictures WHERE userid='" . $this->userid . "' AND reference_id='$element_id' ORDER BY position $order");
        if (empty($elements))
            return '';

        $pictures = array();
        foreach ($elements AS $value) {
            $value['additional_data'] = unserialize($value['additional_data']);
            $pictures[] = $value;
        }

        return $pictures;
    }

    public function update_element($element_id, $array) {
        if (empty($this->userid)) {
            $this->log->warn('userid not defined', $this->userid);
            return false;
        }

        $data = $this->db->getArray("SELECT userid FROM _user_elements WHERE id='$element_id'");
        if ($data[0]['userid'] != $this->userid) {
            $this->log->warn('userid not fit for element userid (' . $element_id . ' / ' . $data[0]['userid'], $this->userid);
            return false;
        }

        $this->db->updateSQL($array, '_user_elements', "id='$element_id'");
        return true;
    }

    public function upload_image($file) {

        if (empty($this->userid)) {
            $this->log->warn('userid not defined', $this->userid);
            return false;
        }

        $this->log->log('file', $file);
        if ($file['error'] == 0) {

            if (!is_dir(RESOURCES . $this->userid . '/')) {
                umask(0);
                mkdir(RESOURCES . $this->userid . '/');
                chmod(RESOURCES . $this->userid . '/', 0777);
            }

            $image = new libraryImages($file['tmp_name'], '');
            $image->imageType();

            $image->setRodzaj('s');
            $img = $image->scale_image(700, 700, false);
            $targetname = RESOURCES . $this->userid . '/' . time() . '_' . rand(9, 999) . '.jpg';
            imagejpeg($img, $targetname, 98);
            imagedestroy($img);
            $this->imageoryginalpath = $file['tmp_name'];
            return $targetname;
        }
    }

    public function add_picture_into_element($elementid, $path, $position = null) {
        if (empty($this->userid)) {
            $this->log->warn('userid not defined', $this->userid);
            return false;
        }

        $data = $this->db->getArray("SELECT userid FROM _user_elements WHERE id='$elementid'");
        if ($data[0]['userid'] != $this->userid) {
            $this->log->warn('userid not fit for element userid (' . $elementid . ' / ' . $data[0]['userid'], $this->userid);
            return false;
        }


        $this->log->warn('in addpicture', $elementid . '/' . $path . '/' . $position);
        if ($position == null) {
            $max = $this->db->getArray("SELECT MAX('position') AS max FROM _elements_pictures WHERE reference_id='$elementid'");
            if (empty($max))
                $position = 1;
            else
                $position = $max[0]['max'] + 1;
        }
        $dane['reference_id'] = $elementid;
        $dane['ip'] = $_SERVER['REMOTE_ADDR'];
        $dane['userid'] = $this->userid;
        $dane['created'] = time();
        $dane['path'] = $path;
        $dane['size'] = filesize($path);
        $dane['ext'] = Files::get_extension_by_filename($path);
        $dane['position'] = $position;
        $dane['name'] = '';
        $dane['additional_data'] = serialize(array());
        $this->db->insertSQL($dane, '_elements_pictures');

        $imageid = $this->db->lastId();
        copy($this->imageoryginalpath, RESOURCES . $this->userid . '/' . $imageid . '.image');

        return $imageid;
    }

    public function deletepicture($pictureid) {
        $this->log->warn('delpicture', $pictureid);
        if (empty($this->userid)) {
            $this->log->warn('userid not defined', $this->userid);
            return false;
        }

        $path = $this->db->getArray("SELECT path FROM _elements_pictures WHERE id='$pictureid' AND userid='" . $this->userid . "'");
        if (empty($path)) {
            $this->log->warn('pictureid and userid does not match', $pictureid);
            return false;
        }
        unlink($path[0]['path']);
        $this->db->Execute("DELETE FROM _elements_pictures WHERE id='$pictureid'");
        return true;
    }

    public function add_picture_additional($pictureid, $additional) {
        $this->log->warn('additional for picture', $pictureid);
        if (empty($this->userid)) {
            $this->log->warn('userid not defined', $this->userid);
            return false;
        }

        $picture = $this->db->getArray("SELECT COUNT(1) AS cnt FROM _elements_pictures WHERE id='$pictureid' AND userid='" . $this->userid . "'");
        if ($picture[0]['cnt'] == 0) {
            $this->log->warn('user is not owner of picture ' . $pictureid, $this->userid);
            return false;
        }

        $dane['additional_data'] = serialize($additional);
        $this->db->updateSQL($dane, '_elements_pictures', "id='$pictureid'");
        return true;
    }

    /**
     * Return id from table auth
     * @return type 
     */
    public function getInternalUserId() {
        return $this->userid;
    }

    public function set_description($element_id, $description) {
        if (empty($this->userid)) {
            $this->log->warn('userid not defined', $this->userid);
            return false;
        }
        
        $data = $this->db->getArray("SELECT userid FROM _user_elements WHERE id='$element_id'");
        if ($data[0]['userid'] != $this->userid) {
            $this->log->warn('userid not fit for element userid (' . $element_id . ' / ' . $data[0]['userid'], $this->userid);
            return false;
        }
        
        
        $desc['description'] = $description;
        $this -> db ->updateSQL($desc, '_user_elements', "id='$element_id'");
        return true;
    }
    
    public function set_status($element_id, $status){
        if (empty($this->userid)) {
            $this->log->warn('userid not defined', $this->userid);
            return false;
        }
        
        $data = $this->db->getArray("SELECT userid FROM _user_elements WHERE id='$element_id'");
        if ($data[0]['userid'] != $this->userid) {
            $this->log->warn('userid not fit for element userid (' . $element_id . ' / ' . $data[0]['userid'], $this->userid);
            return false;
        }
        
        
        $desc['status'] = $status;
        $this -> db ->updateSQL($desc, '_user_elements', "id='$element_id'");
        return true;
    }
    
    /**
     *Get user informations
     * @return type 
     */
    public function getUserInformations(){
        $ans = $this -> db ->getArray("SELECT id, userid, username, first_name, last_name, email, accesstoken FROM auth WHERE id='".$this -> userid."'");
        
        $elements = $this -> db ->getArray("SELECT count(1) AS ilosc FROM _user_elements WHERE userid='".$this -> userid."'");
        $resources = $this -> db ->getArray("SELECT count(1) AS ilosc FROM _user_elements WHERE userid='".$this -> userid."'");
        

        
        return empty($ans) ? '' : array('info' => $ans[0], 'elements' => $elements, 'resources' => $resources);
    }
    
    /**
     *Get info from facebook
     * @param type $userid 
     */
    public function getFacebookData($userid, $accesstoken = ''){
        $facebook = Fcb::getInstance();
        if(empty($accesstoken)){
            $this ->set_fbuserid($userid);
            $info = $this ->getUserInformations();
            if(empty($info['info'])){
                $this -> log ->error('USERID NIE ISTNIEJE', $userid);
                return '';
            }
            $accesstoken = $info['info']['accesstoken'];
        }
        $facebook ->setAccessToken($accesstoken);
        $data = $facebook ->getUserData();
        return empty($data) ? '' : $data;
    }
    
    /**
     *Get userid from selected element
     * @param type $elemnetid
     * @return type 
     */
    public function getUserIdFromElement($elemnetid){
        $userid = $this -> db ->getArray("SELECT userid FROM _user_elements WHERE id='$elemnetid'");
        return empty($userid[0]['userid']) ? '' : $userid[0]['userid'];
    }
    
    public function get_element($element_id){
         $element = $this->db->getArray("SELECT *, FROM_UNIXTIME(created) AS createdh FROM _user_elements WHERE id = '$element_id'");
         return empty($element) ? '' : $element[0];
    }
    
    

}

?>
