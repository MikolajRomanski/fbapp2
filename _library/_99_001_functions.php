<?php
function generate_pagination($active, $pages, $array) {
    libraryLog::_getinstance()->info('Stron: ' . $pages, $array);
    $limit = 3; //po 3 linki cyfrowe
    $firstpage = 1;
    $lastpage = $pages;
    libraryLog::_getinstance()->warn('LASTPAGE', $lastpage);
    //$mmax = 0;
    $mmin = $active;
    //$mmax++;
    $steps = 0;
    $mmin = $active - $limit;
    $mmax = $active + $limit;

    if ($mmin < 1) {
        $diff = abs(1 - $mmin);
        $mmax += $diff;
        if ($mmax > $lastpage)
            $mmax = $lastpage;
    }
    if ($mmax > $lastpage) {
        $diff = abs($lastpage - $mmax);
        $mmin -= $diff;
        if ($mmin < 1)
            $mmin = 1;
    }


    if ($mmax > $lastpage)
        $mmax = $lastpage;
    if ($mmin < 1)
        $mmin = 1;

    /*
      $suma = $limit*2+1;

      if($mmax > $lastpage) $mmax = $lastpage;
      if($mmin < 1) $mmin = 1;
      $diff = $mmax - $mmin;

      $lmin = false;
      $lmax = false;
      $steps = 0;
     *
     */

    /*
      while($diff<$suma && $steps<$limit) {

      if(!$lmax) $mmax++;
      if(!$lmin) $mmin++;
      if($mmax > $lastpage) {
      $mmax = $lastpage;
      $lmax = true;
      }
      if($mmin < 1) {
      $mmin = 1;
      $lmin = true;
      }

      $diff = $mmax - $mmin;
      $steps++;

      }
     *
     */



    $module = $array['module'];
    $action = $array['action'];

    $id1 = $array['id1'];
    $id2 = $array['id2'];
    $id3 = $array['id3'];
    $id4 = $array['id4'];
    $id5 = $array['id5'];
    $querystring = (isset($array['querystring'])) ? $array['querystring'] : '';

    $firstpage_ = $firstpage;
    $firstpage = $active - 1;
    if($firstpage<1) $firstpage = 1;
    

    if ($id1 != '') {
        if ($id1 === true)
            $id1 = $firstpage . ',';
        else
            $id1 = "$id1,";
    }
    if ($id2 != '') {
        if ($id2 === true)
            $id2 = $firstpage . ',';
        else
            $id2 = "$id2,";
    }
    if ($id3 != '') {
        if ($id3 === true)
            $id3 = $firstpage . ',';
        else
            $id3 = "$id3,";
    }
    if ($id4 != '') {
        if ($id4 === true)
            $id4 = $firstpage . ',';
        else
            $id4 = "$id4,";
    }
    if ($id5 != '') {
        if ($id5 === true)
            $id5 = $firstpage . ',';
        else
            $id5 = "$id5,";
    }

    $firstpage = $firstpage_;
    
    $first = "$module/$action/" . $id1 . $id2 . $id3 . $id4 . $id5 . $querystring;

    $id1 = $array['id1'];
    $id2 = $array['id2'];
    $id3 = $array['id3'];
    $id4 = $array['id4'];
    $id5 = $array['id5'];

    $lastpage_ = $lastpage;
    $lastpage = $active+1;
    if($lastpage>$pages) $lastpage = $pages;
    
    if ($id1 != '') {
        if ($id1 === true)
            $id1 = $lastpage . ',';
        else
            $id1 = "$id1,";
    }
    if ($id2 != '') {
        if ($id2 === true)
            $id2 = $lastpage . ',';
        else
            $id2 = "$id2,";
    }
    if ($id3 != '') {
        if ($id3 === true)
            $id3 = $lastpage . ',';
        else
            $id3 = "$id3,";
    }
    if ($id4 != '') {
        if ($id4 === true)
            $id4 = $lastpage . ',';
        else
            $id4 = "$id4,";
    }
    if ($id5 != '') {
        if ($id5 === true)
            $id5 = $lastpage . ',';
        else
            $id5 = "$id5,";
    }
    $lastpage = $lastpage_;
    $last = "$module/$action/" . $id1 . $id2 . $id3 . $id4 . $id5 . $querystring;


    $links = array();
    for ($i = $mmin; $i <= $mmax; $i++) {
        $id1 = $array['id1'];
        $id2 = $array['id2'];
        $id3 = $array['id3'];
        $id4 = $array['id4'];
        $id5 = $array['id5'];
        if ($id1 != '') {
            if ($id1 === true)
                $id1 = $i . ',';
            else
                $id1 = "$id1,";
        }
        if ($id2 != '') {
            if ($id2 === true)
                $id2 = $i . ',';
            else
                $id2 = "$id2,";
        }
        if ($id3 != '') {
            if ($id3 === true)
                $id3 = $i . ',';
            else
                $id3 = "$id3,";
        }
        if ($id4 != '') {
            if ($id4 === true)
                $id4 = $i . ',';
            else
                $id4 = "$id4,";
        }
        if ($id5 != '') {
            if ($id5 === true)
                $id5 = $i . ',';
            else
                $id5 = "$id5,";
        }

        $links[$i] = "$module/$action/" . $id1 . $id2 . $id3 . $id4 . $id5 . $querystring;
    }
    libraryLog::_getinstance()->info('links', $links);
    return array('first' => $first, 'last' => $last, 'links' => $links);
}

/**
 * Check is valid email
 * @author Mikołaj Romański
 * @param string $email
 * @return boolean
 */
function is_valid_email($email, $userid = '') {
    libraryLog::_getInstance() -> info('email pattern', EMAIL_PATTERN);
    if (preg_match(EMAIL_PATTERN, $email) == 0) return false;
    
    if(!empty($userid)){
        $db = Database::_getInstance();
        $cnt = $db ->getArray("SELECT COUNT(1) AS ilosc FROM auth WHERE email='$email' AND userid<>'$userid'");
        if($cnt[0]['ilosc']>0) return false;
    }
    
    return true;
}

function send_mail($to, $temat, $treschtml, $tresctxt = '', $priority = 3) {
    libraryLog::_getInstance()->info("Mail", 'Wysyłam wiadomość ' . $temat);
    libraryLog::_getInstance()->log("email", $to);

    require_once '3rdparty/mailer/class.phpmailer.php';
    $mail = new PHPMailer();
    $mail->AddAddress($to);
    $mail->IsSMTP();
    $mail->From = smtp_mail;
    $mail->FromName = smtp_mail_name;
    $mail->CharSet = 'utf-8';
    $mail->SMTPAuth = smtp_auth;                  // enable SMTP authentication
    $mail->SMTPKeepAlive = false;                  // SMTP connection will not close after each email sent
    $mail->Host = smtp_host; // sets the SMTP server
    $mail->Port = smtp_port;                    // set the SMTP port for the GMAIL server
    $mail->Username = smtp_user; // SMTP account username
    $mail->Password = smtp_password;        // SMTP account password
    $mail->Subject = $temat;
    $mail->AltBody = ($tresctxt == '') ? str_replace('&bsp;', ' ', strip_tags(str_replace("<br/>", "\n", str_replace("<br>", "\n", $treschtml)))) : str_replace('&nbsp;', ' ', strip_tags($tresctxt));
    $mail->MsgHTML($treschtml);
    $mail->Priority = $priority;
    $ok = $mail->Send();
    libraryLog::_getInstance()->info('Status wysłania wiadomości: ' . $ok, $mail->ErrorInfo);
}


function get_galleries($count, $page){
    $db = Database::_getInstance();
    
    $offset = ($page-1)*$count;
    $elements = $db -> getArray("SELECT e.id, e.description, a.first_name, a.last_name, a.userid, a.id AS systemid
        FROM _user_elements AS e, auth AS a
        WHERE e.status = '1' AND a.id=e.userid ORDER BY e.id DESC
        LIMIT $offset, $count");
    $count_ = $db -> getArray("SELECT COUNT(1) AS cnt
        FROM _user_elements AS e, auth AS a
        WHERE e.status = '1' AND a.id=e.userid");
    
    $pages = ceil($count_[0]['cnt']/$count);
    
    $pagination = generate_pagination($page, $pages, array('module' => 'info', 'action' => 'start', 'id1' => true, 'id2' => false, 'id3' => false, 'id4' => false, 'id5' => false));
    libraryLog::_getInstance() -> warn('pagination', $pagination);
    
    return array('elements' => $elements, 'pagination' => $pagination);
}

?>