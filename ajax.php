<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
ob_start();

require_once 'base/config.inc.php';

$dir = opendir('_class');
while($plik = readdir($dir)){
	if(!is_dir('_class/'.$plik)) require_once '_class/'.$plik;
}
closedir($dir);
$dir = opendir('_library');
while($plik = readdir($dir)){
	if(!is_dir('_library/'.$plik)) require_once '_library/'.$plik;
}
closedir($dir);
$ben = Benchmark::_getInstance();
$ben_ = $ben ->Start('Obsługa wywołania');
$_POST = Translate::make_safe_array_post($_POST);
$_GET = Translate::make_safe_array_get($_GET);
libraryLog::_getinstance() ->group_start('Variables','', true);
libraryLog::_getinstance() ->log('GET', $_GET);
libraryLog::_getinstance() ->log('POST', $_POST);

if(!empty($_POST)){
    $plik = fopen(TMPDIR.'post.txt',"a");
    fputs($plik, "\n--".date('Y-m-d H:i:s')."--");
    fputs($plik, print_r($_POST,true));
    fclose($plik);
}

    $plik = fopen(TMPDIR.'get.txt',"a");
    fputs($plik, "\n--".date('Y-m-d H:i:s')."--");
    fputs($plik, print_r($_SERVER["REQUEST_URI"],true));
    fclose($plik);

libraryLog::_getinstance() ->log('_FILE', $_FILES);
libraryLog::_getinstance() ->log('_SESSION', $_SESSION);
libraryLog::_getinstance() ->group_end();

libraryLog::_getInstance() -> log('ajax', $_GET['patch']);
if(!file_exists('ajax/'.Translate::make_filename($_GET['patch']))){
    libraryLog::_getInstance() -> error('ajax', $_GET['patch'].'file not found ');
    $ben ->End($ben_);
    
    exit;
}

include_once 'ajax/'.$_GET['patch'];


$ben ->End($ben_);
$ben ->Raport();
        


?>