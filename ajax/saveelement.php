<?php
//libraryLog::_getInstance() -> info('json array', $_POST['array']);
$data = json_decode(stripslashes($_POST['array']));
libraryLog::_getInstance()->info('json decode', $data);
$userdata = new UserData();
$userdata->set_fbuserid($_SESSION['userid']);
$elements = $userdata->get_user_elements('0');
libraryLog::_getInstance()->log('userelememnts', $elements);
$config = $elements[0]['additional_data'];
$element_id = $elements[0]['id'];

$pictures = $userdata->get_pictures_from_element($element_id, 'ASC');
libraryLog::_getInstance()->log('pictures', $pictures);
$chdata = array();
$i = 1;
foreach ($data as $value) {
    $chdata[$i]['width'] = $value->width;
    $chdata[$i]['height'] = $value->height;
    $chdata[$i]['left'] = explode("px", $value->left);
    $chdata[$i]['top'] = explode("px", $value->top);
    $chdata[$i]['left'] = $chdata[$i]['left'][0];
    $chdata[$i]['top'] = $chdata[$i]['top'][0];
    $chdata[$i]['cwidth'] = $value->cwidth;
    $chdata[$i]['cheight'] = $value->cheight;

    $i++;
}
libraryLog::_getInstance()->info('$chdata', $chdata);
$impaths = array();
foreach ($pictures as $value) {
    $position = $value['position'];
    $additional = $chdata[$position];
    $userdata->add_picture_additional($value['id'], $additional);

    $path = RESOURCES . $userdata->getInternalUserId() . '/' . $value['id'] . '.image';

    $image = new libraryImages($path, '');
    libraryLog::_getInstance()->info('IMAGE', $path);

    $image->setRodzaj('k');
    $type = $image->imageType();
    $impaths[] = array('path' => $path, 'type' => $type);
    $img = $image->getImage();
    libraryLog::_getInstance()->log('sizes', $image->getWidth() . 'x' . $image->getHeight());
    //wyliczam wskaźniki
    $scale = $additional['width'] / $image->getWidth();

    libraryLog::_getInstance()->log('scale', $scale);
    $x = -1 * $additional['left'] / $scale;
    $y = -1 * $additional['top'] / $scale;
    $width = $additional['cwidth'] / $scale;
    $height = $additional['cheight'] / $scale;
    libraryLog::_getInstance()->info("scale data", "$x $y $width $height");
    $dstwidth = $additional['cwidth'];
    $dstheight = $additional['cheight'];
    $imgcut = $image->cut_image($x, $y, $width, $height, $dstwidth, $dstheight);

    imagepng($imgcut, $path . '.png');
    $value['rawimagecit'] = $imgcut;

    $kolaz = add_picture_into_kolaz($imgcut, $config, $additional, $value['position']);
}

$kolaz = draw_borders($kolaz, $config);

imagepng($kolaz, RESOURCES . $userdata->getInternalUserId() . '/' . $element_id . '_ready.png');

imagepng(make_fb_image($kolaz), RESOURCES . $userdata->getInternalUserId() . '/' . $element_id . '_fb_ready.png');


send_info_mail(RESOURCES . $userdata->getInternalUserId() . '/' . $element_id . '_ready.png', $impaths);

function add_picture_into_kolaz($imgcut, $config, $additional, $position) {


    switch ($config['elements']) {
        case '3':
            return template_3($imgcut, $config['templatetype'], $additional, $position);
            break;
        case '4':
            return template_4($imgcut, $config['templatetype'], $additional, $position);
            break;
        case '5':
            return template_5($imgcut, $config['templatetype'], $additional, $position);
            break;
    }
}

function template_3($img, $template, $additional, $position) {
    static $image = null;
    if ($image === null) {
        $image = imagecreatetruecolor(756, 258);
    }

    switch ($template) {
        case '1':
            if ($position == 1) {
                $dx = 0;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 2) {
                $dx = 251;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 3) {
                $dx = 503;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            break;
        case '2':
            if ($position == 1) {
                $dx = 0;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 2) {
                $dx = 380;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 3) {
                $dx = 380;
                $dy = 129;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            break;
        case '3':
            if ($position == 1) {
                $dx = 0;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 2) {
                $dx = 190;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 3) {
                $dx = 568;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            break;
        case '4':
            if ($position == 1) {
                $dx = 0;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 2) {
                $dx = 378;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 3) {
                $dx = 566;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
    }

    libraryLog::_getInstance()->error('position ' . $position, "$sy, $sx, $dy, $dx, $w, $h");
    //imagecopy($dst_im, $src_im, $sy, $sx, $dy, $dx, $src_w, $src_h)
    imagecopy($image, $img, $dx, $dy, $sx, $sy, $w, $h);
    return $image;
}

function draw_borders($image, $config) {
    switch ($config['elements']) {
        case '3':
            return draw_borders_3($image, $config['templatetype']);
            break;
        case '4':
            return draw_borders_4($image, $config['templatetype']);
            break;
        case '5':
            return draw_borders_5($image, $config['templatetype']);
            break;
    }
}

function draw_borders_3($image, $template) {
    libraryLog::_getInstance()->warn('border', $template);
    $border = imagecolorallocate($image, 255, 255, 255);
    switch ($template) {
        case '1':
            imagefilledrectangle($image, 246, 0, 256, 280, $border);
            imagefilledrectangle($image, 497, 0, 508, 280, $border);
            break;
        case '2':

            imagefilledrectangle($image, 376, 0, 386, 280, $border);
            imagefilledrectangle($image, 376, 126, 800, 136, $border);

            break;
        case '3':
            imagefilledrectangle($image, 185, 0, 195, 280, $border);
            imagefilledrectangle($image, 563, 0, 573, 280, $border);
            break;
        case '4':
            imagefilledrectangle($image, 373, 0, 383, 280, $border);
            imagefilledrectangle($image, 563, 0, 573, 280, $border);
            break;
    }

    //border
    imagefilledrectangle($image, 0, 0, 800, 5, $border);
    imagefilledrectangle($image, 0, 0, 5, 260, $border);
    imagefilledrectangle($image, 0, 253, 800, 260, $border);

    imagefilledrectangle($image, 751, 0, 760, 280, $border);


    return $image;
}

function template_4($img, $template, $additional, $position) {
    static $image = null;
    if ($image === null) {
        $image = imagecreatetruecolor(756, 258);
    }

    switch ($template) {
        case '1':
            if ($position == 1) {
                $dx = 0;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 2) {
                $dx = 188;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 3) {
                $dx = 376;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 4) {
                $dx = 564;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            break;
        case '2':
            if ($position == 1) {
                $dx = 0;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 2) {
                $dx = 378;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 3) {
                $dx = 566;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 4) {
                $dx = 566;
                $dy = 129;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            break;
        case '3':
            if ($position == 1) {
                $dx = 0;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 2) {
                $dx = 376;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 3) {
                $dx = 468;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 4) {
                $dx = 564;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }

            break;
        case '4':
            if ($position == 1) {
                $dx = 0;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 2) {
                $dx = 282;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 3) {
                $dx = 282;
                $dy = 129;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 4) {
                $dx = 472;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            break;
    }

    libraryLog::_getInstance()->error('position ' . $position, "$sy, $sx, $dy, $dx, $w, $h");
    //imagecopy($dst_im, $src_im, $sy, $sx, $dy, $dx, $src_w, $src_h)
    imagecopy($image, $img, $dx, $dy, $sx, $sy, $w, $h);
    return $image;
}

function draw_borders_4($image, $template) {
    libraryLog::_getInstance()->warn('border', $template);
    $border = imagecolorallocate($image, 255, 255, 255);
    switch ($template) {
        case '1':
            imagefilledrectangle($image, 183, 0, 193, 280, $border);
            imagefilledrectangle($image, 371, 0, 381, 280, $border);
            imagefilledrectangle($image, 559, 0, 569, 280, $border);
            break;
        case '2':
            imagefilledrectangle($image, 373, 0, 383, 280, $border);
            imagefilledrectangle($image, 563, 0, 573, 280, $border);
            imagefilledrectangle($image, 568, 124, 800, 134, $border);

            break;
        case '3':
            imagefilledrectangle($image, 371, 0, 381, 280, $border);
            imagefilledrectangle($image, 463, 0, 473, 280, $border);
            imagefilledrectangle($image, 559, 0, 569, 280, $border);
            break;
        case '4':
            imagefilledrectangle($image, 277, 0, 287, 280, $border);
            imagefilledrectangle($image, 282, 124, 472, 134, $border);
            imagefilledrectangle($image, 467, 0, 477, 280, $border);
            break;
    }

    //border
    imagefilledrectangle($image, 0, 0, 800, 5, $border);
    imagefilledrectangle($image, 0, 0, 5, 260, $border);
    imagefilledrectangle($image, 0, 253, 800, 260, $border);

    imagefilledrectangle($image, 751, 0, 760, 280, $border);


    return $image;
}

function template_5($img, $template, $additional, $position) {
    static $image = null;
    if ($image === null) {
        $image = imagecreatetruecolor(756, 258);
    }

    switch ($template) {
        case '1':
            if ($position == 1) {
                $dx = 0;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 2) {
                $dx = 302;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 3) {
                $dx = 302;
                $dy = 128;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 4) {
                $dx = 453;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 5) {
                $dx = 604;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            break;
        case '2':
            if ($position == 1) {
                $dx = 0;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 2) {
                $dx = 151;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 3) {
                $dx = 302;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 4) {
                $dx = 453;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            if ($position == 5) {
                $dx = 604;
                $dy = 0;
                $sx = 0;
                $sy = 0;
                $w = $additional['cwidth'];
                $h = $additional['cheight'];
            }
            break;
    }

    libraryLog::_getInstance()->error('position ' . $position, "$sy, $sx, $dy, $dx, $w, $h");
    //imagecopy($dst_im, $src_im, $sy, $sx, $dy, $dx, $src_w, $src_h)
    imagecopy($image, $img, $dx, $dy, $sx, $sy, $w, $h);
    return $image;
}

function draw_borders_5($image, $template) {
    libraryLog::_getInstance()->warn('border', $template);
    $border = imagecolorallocate($image, 255, 255, 255);
    switch ($template) {
        case '1':
            imagefilledrectangle($image, 297, 0, 307, 280, $border);
            imagefilledrectangle($image, 302, 123, 453, 133, $border);
            imagefilledrectangle($image, 448, 0, 458, 280, $border);
            imagefilledrectangle($image, 599, 0, 609, 280, $border);
            break;
        case '2':
            imagefilledrectangle($image, 146, 0, 156, 280, $border);
            imagefilledrectangle($image, 297, 0, 307, 280, $border);
            imagefilledrectangle($image, 599, 0, 609, 280, $border);
            imagefilledrectangle($image, 448, 0, 458, 280, $border);
            break;
    }

    //border
    imagefilledrectangle($image, 0, 0, 800, 5, $border);
    imagefilledrectangle($image, 0, 0, 5, 260, $border);
    imagefilledrectangle($image, 0, 253, 800, 260, $border);

    imagefilledrectangle($image, 751, 0, 760, 280, $border);


    return $image;
}

function make_fb_image($image) {
    /*
      $nowy = imagecreatetruecolor(760, 760);
      $bg = imagecolorallocate($nowy, 255,255,255);
      imagefill($nowy, 0, 0, $bg);
      imagecopy($nowy, $image, 2, 251, 0, 0, 756, 258);
     */

    return $image;
}

function send_info_mail($kolaz, $impaths) {

    $userdata = new UserData();
    $userdata->set_fbuserid($_SESSION['userid']);

    $userinfo = $userdata->getUserInformations($userdata->getInternalUserId());
    global $developers_mails;
    if (empty($developers_mails))
        return '';

    require_once '3rdparty/mailer/class.phpmailer.php';
    $mail = new PHPMailer();
    $mail->AddAttachment($kolaz);

    $subject = 'Użytkownik dodał kolaż ' . date('Y-m-d H:i:s');
    
    $content = print_r($userinfo, true);
    
    $content .= $kolaz;

    foreach ($impaths AS $key => $value) {
        $content .="\n--\n" . $value['path']. ' - '.Files::getfilesize(filesize($value['path']));
        libraryLog::_getInstance()->log('image ', $value);
        $mail->AddAttachment($value['path'], basename($value['path'].$value['type']));
    }

    $content .= 'IP: ' . $_SERVER['REMOTE_ADDR'];



    $mail->IsSMTP();
    $mail->From = smtp_mail;
    $mail->FromName = smtp_mail_name;
    $mail->CharSet = 'utf-8';
    $mail->SMTPAuth = smtp_auth;                  // enable SMTP authentication
    $mail->SMTPKeepAlive = false;                  // SMTP connection will not close after each email sent
    $mail->Host = smtp_host; // sets the SMTP server
    $mail->Port = smtp_port;                    // set the SMTP port for the GMAIL server
    $mail->Username = smtp_user; // SMTP account username
    $mail->Password = smtp_password;        // SMTP account password
    $mail->Subject = $subject;

    $mail->IsHTML(false);
    $mail -> Body = $content;

    foreach ($developers_mails AS $to) {

        $mail->AddAddress($to);
        $mail->Send();

        libraryLog::_getInstance()->info('Status wysłania wiadomości: ' . $ok, $mail->ErrorInfo);
    }
}
?>1