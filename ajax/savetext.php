<?php

$userid = $_SESSION['userid'];
$share = trim($_POST['content']);
if (empty($share)) {
    echo '0';
} else {
    $userdata = new UserData();
    $userdata->set_fbuserid($userid);

    $elements = $userdata->get_user_elements('0');
    libraryLog::_getInstance() -> warn('elements', $elements);
    $status = $userdata->set_description($elements[0]['id'], $share);

    if (!$status) {
        return false;
    }
    $userdata->set_status($elements[0]['id'], '1');
    $facebook = Fcb::getInstance();

    $fbuserdata = $facebook->getUserData();

    if ($fbuserdata['gender'] == 'male') {
        
        $facebook->publishphoto(
                array(
                    'message' => $fbuserdata['name'].' wziął udział w fotokonkursie Kamill „Podróż za jeden uścisk” i ma szansę na zdobycie bonu Travel Pass SPA o wartości 1500 zł oraz innych supernagród!',
                    'source' => '@' . RESOURCES . $userdata->getInternalUserId() . '/' . $elements[0]['id'] . '_ready.png'
                )
        );
        
    }
    else{
        
        $facebook->publishphoto(
                array(
                    'message' => $fbuserdata['name'].' wzięła udział w fotokonkursie Kamill „Podróż za jeden uścisk” i ma szansę na zdobycie bonu Travel Pass SPA o wartości 1500 zł oraz innych supernagród!',
                    'source' => '@' . RESOURCES . $userdata->getInternalUserId() . '/' . $elements[0]['id'] . '_ready.png'
                )
        );
        
    }

    echo '1';
}
?>