<?php
if (isset($_FILES['file'])) {
    if ($_FILES['file']['error'] != 0) {
        echo json_encode(array("success" => false));
    } else {
        $userdata = new UserData();
        $userdata ->set_fbuserid($_SESSION['userid']);
        $path = $userdata -> upload_image($_FILES['file']);
        
        if($path==false){
            echo json_encode(array("success" => false, 'path'=> $path));
        }
        else{
            $pictid = explode("pict", $_POST['activepicture']);
            $pictid = $pictid[1];
            libraryLog::_getInstance() -> log('activepicture', $pictid);
            $elements = $userdata ->get_user_elements('0');
            $pictures = $userdata ->get_pictures_from_element($elements[0]['id']);
            libraryLog::_getInstance() -> log('pictures', $pictures);
            if(!empty($pictures)){
                $delpictureid = null;
                foreach($pictures as $value){
                    if($value['position'] == $pictid) $delpictureid = $value['id'];
                }
                if($delpictureid!=null) $userdata -> deletepicture($delpictureid);
            }
            libraryLog::_getInstance() -> warn('add picture into gallery', $pictid);
            $userdata -> add_picture_into_element($elements[0]['id'], $path, $pictid);
            
            list($width, $height) = getimagesize($path);
            echo json_encode(array("success" => true, 'path'=> $path, 'pictureid' => $pictid, 'width' => $width, 'height' => $height));
        }
        
    }
} else {
    echo json_encode(array("success" => false));
}
?>