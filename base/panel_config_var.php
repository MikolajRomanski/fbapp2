<?php

define('developer', 'support@doromi.net');
define('admin', 'mikolaj@doromi.net');

//langs
/*
 * Hungary:
Czech Republic:
Slovakia:
Slovenia:
Bosnia i Herzegovina:
Croatia:
 */
$langs = array('hu', 'cz', 'sk', 'sl', 'ba', 'hr', 'en');
define('DEFAULT_LANG', 'en');

define('RESOURCES', 'resources/');


/*ustala wiele szczegółów
 * - debugowanie firebug
 * - kompresja css
 * - kompresja js
 * - wyrzucanie błędów na publicznej stronie (output buffer)
 * - email z raportem błędów
 * 
 * wartości i działania te można ręcznie zmienić przy odpowiednich zmiennych
*/
define('PRODUCTION_VERSION', false);

//facebook w pliku config_host - zależnym od konfiguracji na host
//
//
//do testów działania. 
define('FB_FORCE_ID', false);

/**
 *firephp
 */
define('DEBUG', false);
/**
 *raport błędu na email 
 */
define('EMAIL_REPORT', PRODUCTION_VERSION);
/**
 *Kompresuj CSS 
 */
define('CSS_COMPRESS', false);
/**
 *Kompresuj JS 
 */
define('JS_COMPRESS', false);

//compress output
define('COMPRESS_CODE', false);

/**
 *Debuguj Smarty - używać ostrożnie
 */
define('DEBUGSMARTY', false);

/*poniżej rzadko kiedy potrzebne są modyfikacje*/

define('TMPDIR', 'TMPDIR/');


define('TEMPLATEDIR', 'templates/panel/');
define('ERRORDIR', TMPDIR.'ErroRS/');
define('TEMPLATESC', TMPDIR.'template_a/');
define('TEMPLATESA', TMPDIR.'template_a/');
define('CSSDIR', TMPDIR.'_cssdir/');
define('JSDIR', TMPDIR.'_jsdir/');

define('JS_ONE_FILE', false);
define('CSS_ONE_FILE', false);

define('PDODRIVER', 'mysql');

define("EMAIL_PATTERN", '/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i');
$developers_mails = array('support@doromi.net');
?>