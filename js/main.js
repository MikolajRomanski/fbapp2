$(document).ready(function(){
    dynamicload();
    setInterval("check_if_can_save()", 1000);
    if($('.vhid').length>0) $('.vhid').css('display', 'none');
    paginatealign();
    blank();
    scrollafterload();
});


function blank(){
    $('.blank').attr('target', '_blank');
}

function dynamicload(){
    clickpicture();
    click2editpicture();
    //selectlastpreview();
    cleartextarea();
}
var activestep = 'step1';
var activetemplate = '3';
function loadcnt(step, ref){
    cansave = false;
    hidetipsy();
    switch(step){
        case 'step1':
            loadstep1();
            break;
        case 'step2':
            
            loadstep2();
            
            break;
        case 'step3':
            
            loadstep3();
            break;
        case 'step4':
            
            loadstep4();
            break;
    }
    if(ref!='') {   
        activetemplate=ref;
        actelement = ref;
    }
}
var forminited = false;
function loadstep1(){
    actelement = '';
    if($('.cntslide>.'+activestep).length>0){
        
    
        $('.cntslide>.'+activestep).animate({
            opacity: 0, 
            marginLeft: -810
        }, 400, function(){
        
            var htmlc = $('#step1').html();
            $('.cntslide').css('opacity', 0);
            $('.cntslide').html(htmlc);
            $('.cntslide>.step1').css('margin-left', '-810px');
            $('.cntslide>.step1').css('opacity', 0);
            $('.cntslide').css('opacity', 1);
            $('.cntslide>.step1').animate({
                opacity: 1, 
                marginLeft: 0
            }, 400);
            dynamicload();
            if(!forminited){
                Custom.init();
                forminited = true;
            }
            activestep = 'step1'; 
        });
    
    }
    else{
        var htmlc = $('#step1').html();
        $('.cntslide').css('opacity', 0);
        $('.cntslide').html(htmlc);
        $('.cntslide>.step1').css('margin-left', '-810px');
        $('.cntslide>.step1').css('opacity', 0);
        $('.cntslide').css('opacity', 1);
        $('.cntslide>.step1').animate({
            opacity: 1, 
            marginLeft: 0
        }, 400);
        dynamicload();
        if(!forminited){
            Custom.init();
            forminited = true;
        }
        activestep = 'step1'; 
    }
}

function loadstep2(){
    
    $('.cntslide>.'+activestep).animate({
        opacity: 0, 
        marginLeft: -810
    }, 400, function(){
        
        var htmlc = $('#step2>.els_'+activetemplate).html();
        
        $('.cntslide').css('opacity', 0);
        $('.cntslide').html(htmlc);
        $('.cntslide>.step2').css('margin-left', '-810px');
        $('.cntslide>.step2').css('opacity', 0);
        $('.cntslide').css('opacity', 1);
        $('.cntslide>.step2').animate({
            opacity: 1, 
            marginLeft: 0
        }, 400);
        dynamicload();
        //load active configuration
        if(activetemplate == actelement){
            el = $('.cntslide>.step2').find('.ccont'+templatetype);
            $('.ps.act').removeClass('act');
            el.parent().addClass('act');
            el.click();
        }
        activestep = 'step2';
    });
}

function loadstep3(){

    $('.cntslide>.'+activestep).animate({
        opacity: 0, 
        marginLeft: -810
    }, 400, function(){
        
        var htmlc = $('#step3').html();
        $('.cntslide').css('opacity', 0);
        $('.cntslide').html(htmlc);
        $('.cntslide>.step2').css('margin-left', '-810px');
        $('.cntslide>.step2').css('opacity', 0);
        $('.cntslide').css('opacity', 1);
        $('.cntslide>.step2').animate({
            opacity: 1, 
            marginLeft: 0
        }, 400);
        dynamicload();
        if(!forminited){
            Custom.init();
            forminited = true;
        }
        activestep = 'step3';
    });
}

function loadstep4(){
    document.location.href="galeria/thanks/";
}

var proportional = 2.043360434;
var stemplaterequest = null;
function clickpicture(){
    $('.ccont').unbind('click');
    $('.ccont').click(function(){
        $('.ccont').unbind('click');
        $('.ps.act').removeClass('act');
        $(this).parent().addClass('act');
        
        
        if(stemplaterequest!== null){
            stemplaterequest.abort();
        }
        var elid = $('.act').children('.ccont').children('input').val();
        stemplaterequest = $.post('ajax/save_template.php', {
            elements: activetemplate, 
            templatetype: elid
        }, function(){
            stemplaterequest = null;
        });
        
        
        $('.pic'+activetemplate+'cn').animate({
            opacity: 0
        }, 400, function(){
            
            
            var viewelement = $(this); 
            viewelement.html('');
            
            $('.ps.act').children('div').each(function(){
                if(!$(this).hasClass('ccont')){
                    var efloat = $(this).css('float');
                    var ewidth = $(this).width();
                    var eheight = $(this).height();
                    var eleft = $(this).css('margin-left');
                    var etop = $(this).css('margin-top');
                    
                    var nwidth = Math.floor(ewidth*proportional);
                    var nheight = Math.floor(eheight*proportional);
                    
                    
                    
                    etop = etop.replace(/px/,'');
                    eleft = eleft.replace(/px/, '');
                    
                    
                    
                    eleft = eleft*proportional;
                    etop = etop*proportional;
                    
                    
                    
                    var eclass = $(this).attr('class');
                    //
                    /*if(picture[activetemplate][$(this).attr('class')] == undefined){
                        var el = "<div id='picturepreview_"+$(this).attr('class')+"' style='float: "+efloat+"; width: "+nwidth+"px; height: "+nheight+"px;' class='"+eclass+"'></div>";
                    }
                    else{*/
                    var el = "<div id='picturepreview_"+$(this).attr('class')+"' style='float: "+efloat+"; width: "+nwidth+"px; height: "+nheight+"px; margin-top: "+etop+"px; margin-left: "+eleft+"px;' class='"+eclass+"'><div class='abupl' style='width: "+nwidth+"px; height: "+nheight+"px;'></div></div>";
                    //}
                    
                    viewelement.append(el);
                    
                }
            });
            $('.cntslide>div>.pictmpl>.previewcontainer').after("<div class='clear' ></div>");
            bordersizes(activetemplate, elid);
            viewelement.animate({
                opacity: 1
            },400);
            dynamicload();
        });
        
    });
}
var uploadelementclass = '';
function click2editpicture(){
    $('.abupl').unbind('click');
    $('.abupl').click(function(){
        if(uploading) return false;
        $('.pagetitle>h2').html("Wgraj zdjecia / <span class='smlett'>500 znaków</span>");
        $('.cntslide>.step2>.pictmpl>.ps').animate({
            opacity:0
        }, 400, function(){
            $(this).remove();
        });
        $('.abupl').unbind('click');
        $('#pictureedit').remove();
        var elclass = $(this).parent().attr('class');
        var elwidth = $(this).width();
        var elheight = $(this).height();
        var content = $('.cntslide>div>.pictmpl>.description').html();
        el = $('.cntslide>div>.pictmpl>.description')
        $('.cntslide>div>.pictmpl>.description').animate({opacity: 0}, 400, function(){
            el.remove();
        });
        
        if($('.disdis').length>0){
            $('.cntslide>div>.pictmpl>.description').remove();
        }
        $('.cntslide').children('.step2').children('.pictmpl').children('.previewcontainer').after("<div class='description nohide disdis'>"+content+"</div><div id='pictureedit'><div class='uploader'><form enctype='multipart/form-data' method='post' action='ajax/upload_picture.php' id='uplfile'><input type='hidden' value='"+elclass+"' name='activepicture' id='activepicture'/><input type='file' name='file'/><input type='hidden' name='width' value='"+elwidth+"'/><input type='hidden' name='height' value='"+elheight+"'/></form></div><div id='previewarea'></div></div>");        
        $('.cntslide>div>.pictmpl>.description.nohide').animate({opacity: 1}, 800);
        $('#uplfile>input').change( function()
        {
            if( $(this).val() !='' ){
                var eclass = $('#activepicture').val();
                
                uploadelementclass = eclass;
                $('.'+eclass).children('.abupl').addClass('loading');
                $('#uplfile').submit();
            }
        });
        setTimeout("click2editpicture()", 400);
        iframeupload();
        if($(this).parent().children('img').length>0){
            var imgel = $(this).parent().children('img');
            var imgsrc = imgel.attr('src');
            $('#activeeditor').removeAttr('id');
            imgel.attr('id', 'activeeditor');
            targetwidth = imgel.parent().width();
            targetheight = imgel.parent().height();
            
            $('#previewarea').html('');
            $('#previewarea').append("<img src='"+imgsrc+"' style='width: "+700+"px;'/>");
            previewareaheight = $('#previewarea').children('img').height();
            previewareawidth= $('#previewarea').children('img').width();
            $('#previewarea').children('img').Jcrop({
                onChange: showPreview,
                onSelect: showPreview,
                aspectRatio: targetwidth/targetheight
            });
        }
        fbobject.Canvas.scrollTo(0,1000);
    });
}

var uploading = false;
function iframeupload(){
    $('form#uplfile').iframePostForm
    ({
        json : true,
        post : function ()
        {
            uploading = true;
            $('.abupl').css('cursor', 'wait');
            var message;
		
            $('form#uplfile').animate({
                opacity: 0
            }, 400);
			
            if ($('input[type=file]').val().length)
            {
                
            }	
            else
            {
                		
                return false;
            }
        },
        complete : function (response)
        {
            uploading = false;
            $('.abupl').css('cursor', 'pointer');
            $('form#uplfile').animate({
                opacity: 1
            }, 400);
            if(response.success) {
                
                show_preview(response);
                check_if_can_save();
                setTimeout("fbscrolldown()", 600);
            }
            else {
                $('.loading').removeClass('loading');
            }
        }
    });
}

function fbscrolldown(){
    fbobject.Canvas.scrollTo(0,1500);
}

var previewareawidth = 0;
var previewareaheight = 0;
var targetwidth = 0;
var targetheight = 0;
function show_preview(response){
    $('#activeeditor').removeAttr('id');
    //console.log(response);
    var pictureid = response.pictureid;
    var picturepath = response.path;
    $('#picturepreview_pict'+pictureid).children('.abupl').removeClass('loading');
    var picture = $('#picturepreview_pict'+pictureid).children('img');
    if(picture.length>0){
        picture.animate({
            opacity: 0
        }, 300, function(){
            $(this).remove();
            $('#picturepreview_pict'+pictureid).append("<img id='activeeditor' src='"+picturepath+"' style='width: "+response.width+"px; height: "+response.height+"px;'/>");
        });
    }
    else{
        $('#picturepreview_pict'+pictureid).append("<img id='activeeditor' src='"+picturepath+"' style='width: "+response.width+"px; height: "+response.height+"px;'/>");
    }
    
    $('#activeeditor').animate({
        opacity: 1
    }, 1000);
    
    previewareaheight = response.height;
    previewareawidth = response.width;
    targetwidth = $('#picturepreview_pict'+pictureid).children('.abupl').width();
    targetheight = $('#picturepreview_pict'+pictureid).children('.abupl').height();
    $('#previewarea').html('');
    $('#previewarea').append("<img src='"+picturepath+"' style='width: "+response.width+"px; height: "+response.height+"px;'/>");
    $('#previewarea').children('img').Jcrop({
        onChange: showPreview,
        onSelect: showPreview,
        aspectRatio: targetwidth/targetheight
    });
}

function showPreview(coords)
{
    var rx = targetwidth / coords.w;
    var ry = targetheight / coords.h;

    $('#activeeditor').css({
        width: Math.round(rx * previewareawidth) + 'px',
        height: Math.round(ry * previewareaheight) + 'px',
        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        marginTop: '-' + Math.round(ry * coords.y) + 'px'
    });
}

var cansave = false;
function check_if_can_save(){
    if(cansave) return false;
    if(actelement == undefined) return false;
    if(actelement=='') return false;
    var els = $('.previewcontainer').find('img');
    if(els.length>=actelement){
        cansave = true;
        $('.cntslide>.step2>.savecontentnow').css('opacity', 0).css('display','block');
        $('.cntslide>.step2>.savecontentnow').animate({
            opacity: 1
        }, 400);
    }
    return true;
}

function savecontentnow(){
    var els = $('.previewcontainer').find('img');

    var pictures = new Array();
    els.each(function(){
        var eclass = $(this).parent().attr('class');

        pictures.push({
            eclass: eclass, 
            width: $(this).width(),
            height: $(this).height(), 
            left: $(this).css('margin-left'),
            top: $(this).css('margin-top'),
            cwidth: $(this).parent().width(),
            cheight: $(this).parent().height()
        });
        
        
    });
    
    
    $.post('ajax/saveelement.php', {
        array: JSON.stringify(pictures)
    }, function(){
        loadcnt('step3','');
    });
}
    
    
function cleartextarea(){
    $('.textarea>textarea').focus(function(){
        
        $(this).removeAttr('style');
    });
    //transparent url('css/img/pencil.png') no-repeat 0 0
    $('.textarea>textarea').blur(function(){
        
        if($(this).val() == ''){
            $(this).css('background', "transparent url('css/img/pencil.png') no-repeat 0 0");
        }
    });
}

function submitfile(){
    var share = $('.textarea>textarea').val();
    $('.cntslide>.step3>.acreg, .cntslide>.step3>.share').tipsy('hide');
    if( $('#accreg').is(':checked') == false ){
        $('.cntslide>.step3>.acreg, .cntslide>.step3>.share').tipsy({
            gravity: 'se', 
            trigger: 'manual', 
            title: 'rel'
        });
        $('.cntslide>.step3>.acreg, .cntslide>.step3>.share').tipsy('show');
        setTimeout("hidetipsy()", "4000");
    }
     
    if(share=='') return false;
     
    $.post('ajax/savetext.php', {
        content: share
    }, function(dane){
        if(dane==1) {
            document.location.href="galeria/complete/";
            loadcnt('step4','');
        }
    });
     
}

function hidetipsy(){
    $('.cntslide>.step3>.acreg, .cntslide>.step3>.share').tipsy('hide');
}

var bordersizes_ = '';
function bordersizes(elements, template){
    var sizes = new Array();
    sizes[3] = new Array();
    sizes[4] = new Array();
    sizes[5] = new Array();
    
    sizes[3][1] = [
    [246, 0, 256, 280],
    [497, 0, 508, 280]
    ];
    sizes[3][2] = [
    [376, 0, 386, 280],
    [376, 126, 754, 136]
    ];
    sizes[3][3] = [
    [185, 0, 195, 280],
    [563, 0, 573, 280]
    ];
    sizes[3][4] = [
    [373, 0, 383, 280],
    [563, 0, 573, 280]
    ];
    
    sizes[4][1] = [
    [183, 0, 193, 280],
    [371, 0, 381, 280],
    [559, 0, 569, 280]
    ];
    sizes[4][2] = [
    [373, 0, 383, 280],
    [563, 0, 573, 280],
    [568, 124, 800, 134]
    ];
    sizes[4][3] = [
    [371, 0, 381, 280],
    [463, 0, 473, 280],
    [559, 0, 569, 280]
    ];
    sizes[4][4] = [
    [277, 0, 287, 280],
    [282, 124, 472, 134],
    [467, 0, 477, 280]
    ];
    
    sizes[5][1] = [
    [297, 0, 307, 280],
    [302, 123, 453, 133],
    [448, 0, 458, 280],
    [599, 0, 609, 280]
    ];
    sizes[5][2] = [
    [146, 0, 156, 280],
    [297, 0, 307, 280],
    [599, 0, 609, 280],
    [448, 0, 458, 280]
    ];
    var bordersizes_ = sizes;
    /*console.log(sizes);
    console.log(elements, template);
    console.log(sizes[3][1]);*/
    
    
    
    for(i in sizes[elements][template]){
        if(i == undefined ) continue;
        mleft = sizes[elements][template][i][0]
        mtop = sizes[elements][template][i][1]
        
        width = sizes[elements][template][i][2] - sizes[elements][template][i][0];
        height = sizes[elements][template][i][3] - sizes[elements][template][i][1];
        
        $('.cntslide>div>.pictmpl>.previewcontainer').prepend("<div class='bor' style='margin-top: "+mtop+"px; margin-left: "+mleft+"px; width: "+width+"px; height: "+height+"px;'></div>");
    }
    $('.cntslide>div>.pictmpl>.previewcontainer').prepend("<div class='bor' style='margin-top: "+0+"px; margin-left: "+0+"px; width: "+754+"px; height: "+5+"px;'></div>");
    $('.cntslide>div>.pictmpl>.previewcontainer').prepend("<div class='bor' style='margin-top: "+0+"px; margin-left: "+0+"px; width: "+5+"px; height: "+257+"px;'></div>");
    $('.cntslide>div>.pictmpl>.previewcontainer').prepend("<div class='bor' style='margin-top: "+252+"px; margin-left: "+0+"px; width: "+754+"px; height: "+5+"px;'></div>");
    $('.cntslide>div>.pictmpl>.previewcontainer').prepend("<div class='bor' style='margin-top: "+0+"px; margin-left: "+749+"px; width: "+5+"px; height: "+257+"px;'></div>");
}

function paginatealign(){
    var ulwidth = 0;
    $('.lpaginate').each(function(){
        ulwidth += $(this).width();   
        ulwidth += 11;
    });


    $('.upaginate').css('width',ulwidth+'px');
    $('.upaginate').css('margin', 'auto');
    $('.upaginate').css('display', 'block');
    setTimeout("paginatealign()", 1500);
}

function showtemplateupload(){
    
    loadcnt('step2', activetemplate);
}

function scrollafterload(){
    
    return true;
}