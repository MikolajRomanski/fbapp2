$(document).ready(function(){
    $('.blur').blurjs();
    clicktoroll();
    $("a.prettyPhoto").prettyPhoto({
        theme:'light_square'
    })
});

function clicktoroll(){
    $('.loadresources').click(function(){
        $('.loadresources').unbind('click');
        var id = $(this).children('input').val();
        $('.loadresources').animate({opacity: 0}, 400);
        $(this).stop().animate({opacity: 0.6}, 300);
        document.location.href=abs+'manage/mainpage/listelements/'+id;
    }); 
    
    $('.loadelement').click(function(){
        $('.loadelement').unbind('click');
        var id = $(this).children('input').val();
        $('.loadelement').animate({opacity: 0}, 400);
        $(this).stop().animate({opacity: 0.6}, 300);
        document.location.href=abs+'manage/mainpage/listresources/'+id;
    }); 
}