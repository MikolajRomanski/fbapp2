<?php

class galeriaClass {

    /**
     *
     * @var libraryLog
     */
    private $log;

    /**
     *
     * @var Fcb
     */
    private $facebook;

    /**
     *
     * @var Smarty
     */
    private $smarty;

    public function __construct() {
        HTTP::Redirect(URLABS.'mainpage/start/');
        $this->log = libraryLog::_getInstance();
        $this->facebook = Fcb::getInstance();
        $this->smarty = UseSmarty::getInstance();

        $this->smarty->assign('is_auth', $this->facebook->is_authorized());
        $this->smarty->assign('is_fan', $this->facebook->is_fan());

        if (!$this->facebook->is_authorized()) {
            $this->smarty->assign('authurl', $this->facebook->getLoginUrl());
        }
        else
            $this->smarty->assign('authurl', '');

        if (!$this->facebook->is_fan()) {
            $this->force_fan();
        }
    }

    public function indexAction() {
        HTTP::Redirect(URLABS.'mainpage/start/');
        $userdata = $this->facebook->getUserData();
        $userid = $userdata['id'];
        $_SESSION['userid'] = $userid;

        $this -> smarty -> assign('userdata', $userdata);
        $this->smarty->assign('fanbacklink', '/galeria/');
        $this->smarty->assign('pagetitle', 'Stwórz kolaż');

        $user = new UserData();
        $user->set_fbuserid($_SESSION['userid']);
        $elements = $user->get_user_elements('0');

        if (empty($elements)) {
            $user->create_element();
            $elements = $user->get_user_elements('0');
        }
        
        if (empty($elements[0]['additional_data'])) {
            $elements = array(array('elements' => '3', 'templatetype' => '4'));
        }
 
        $this->log->log('user elements', $elements);
        $this->smarty->assign('elements', $elements);
        //get pictures
        $pictures = $user->get_pictures_from_element($elements[0]['id']);
        $this->log->log('pictures', $pictures);
        $this->smarty->assign('pictures', $pictures);
    }

    public function joinAction() {
        $this->smarty->assign('fanbacklink', '/galeria/');
        $this->smarty->assign('pagetitle', 'Galeria fotografii');
        $this->smarty->assign('template', 'm_gallery_index.tpl');
    }

    public function force_fan() {
        $this->smarty->assign('template', 'm_info_force_fan.tpl');
    }
    
    public function thanksAction() {
        $this->smarty->assign('fanbacklink', '/galeria/');
        $this->smarty->assign('pagetitle', 'Dziękujemy');
        
        
    }
    
}

?>