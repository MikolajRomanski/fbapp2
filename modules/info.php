<?php

class infoClass {

    /**
     *
     * @var libraryLog
     */
    private $log;

    /**
     *
     * @var Fcb
     */
    private $facebook;

    /**
     *
     * @var Smarty
     */
    private $smarty;

    public function __construct() {
        $this->log = libraryLog::_getInstance();
        $this->facebook = Fcb::getInstance();
        $this->smarty = UseSmarty::getInstance();

        $this->smarty->assign('is_auth', $this->facebook->is_authorized());
        $this->smarty->assign('is_fan', $this->facebook->is_fan());

        if (!$this->facebook->is_authorized()) {
            $this->smarty->assign('authurl', $this->facebook->getLoginUrl());
            
            
            
        }
        else{
            $this->smarty->assign('authurl', '');
            $permissions = $this->facebook->RawFB()->api("/me/permissions");

            $datas = print_r($permissions['data'][0], true);
            $pl = fopen(TMPDIR.'_miksec_'.date('Y-m-d').'.txt', "a");
            
            $server = $_SERVER['REMOTE_ADDR'];
            $data = date('Y-m-d H:i:s');
            fputs($pl, "\n--\n$server - $data\n".$datas);
            fclose($pl);
            
            if (!array_key_exists('user_likes', $permissions['data'][0])) {
                //HTTP::Redirect(URLABS . $_SESSION['lang'] . '/info/forceperms/');
                $this->forceperms();
            }
            
        }

        if (!$this->facebook->is_fan()) {
            $this->force_fan();
        }
    }

    public function startAction() {
        $timestamp = time();
        $this->smarty->assign('fanbacklink', '/info/start/1,'.$timestamp.'#backfromrules');
        $galleries = get_galleries(3, empty($_GET['id1']) ? 1 : $_GET['id1']);

        $this->smarty->assign('galleries', $galleries['elements']);
        $this->smarty->assign('paginate', $galleries['pagination']);
        $this->smarty->assign('active_page', empty($_GET['id1']) ? 1 : $_GET['id1']);
        $this->smarty->assign('pagetitle', 'Galeria fotografii');
        $this -> smarty -> assign('actm', 'galeria');
    }

    public function force_fan() {

        if ($this->facebook->is_authorized()) {
            //$permissions = $this->facebook->RawFB()->api("/me/permissions");
/*
            $datas = print_r($permissions['data'][0], true);
            $pl = fopen(TMPDIR.'_miksec_'.data('Y-m-d').'.txt', "a");
            
            $server = $_SERVER['REMOTE_ADDR'];
            $data = data('Y-m-d H:i:s');
            fputs($pl, "\n--\n$server - $data\n".$datas);
            fclose($pl);
            */
            /*
            if (!array_key_exists('user_likes', $permissions['data'][0])) {
                //HTTP::Redirect(URLABS . $_SESSION['lang'] . '/info/forceperms/');
                $this->forceperms();
            }*/
        }
        $this->smarty->assign('template', 'm_info_force_fan.tpl');
    }

    public function zasadyAction() {
        $this->smarty->assign('actm', 'zasady');
        $this->smarty->assign('fanbacklink', '/info/zasady/#topgal');
        $this->smarty->assign('pagetitle', 'Zasady konkursu');
    }

    public function nagrodyAction() {
        $this->smarty->assign('actm', 'nagrody');
        $this->smarty->assign('fanbacklink', '/info/nagrody/#topgal');
        $this->smarty->assign('pagetitle', 'Nagrody do zdobycia');
    }

    public function forceperms() {
        $this->smarty->assign('authurl', $this->facebook->getLoginUrl());
    }

}

?>