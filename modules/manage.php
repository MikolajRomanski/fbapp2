<?php
class mainpageClass{
    public function __construct(){
        
    }
    public function startAction(){
        $galleries = get_galleries(3,empty($_GET['id1']) ? 1 : $_GET['id1']);
        $smarty = UseSmarty::getInstance();
        $smarty -> assign('galleries', $galleries['elements']);
        $smarty -> assign('paginate', $galleries['pagination']);
        $smarty -> assign('active_page', empty($_GET['id1']) ? 1 : $_GET['id1']);
        $smarty->assign('pagetitle', 'Galeria fotografii');
    }
}

?>