<?php

class loginClass {

    /**
     * singleton of smarty object
     * @var UseSmarty
     */
    private $smarty = null;
    public function __construct() {
        $this -> smarty = UseSmarty::getInstance();
        $js = new objJS();
        $js->add('login.js', 'js/panel');
        $this->smarty->assign('additionaljs', $js->generate());

        $css = new objCSS();
        $css->add('login.css', 'css/panel/');
        $this->smarty->assign('additionalcss', $css->generate());
    }

    public function indexAction() {
        if(!empty($_POST['email']) && !empty($_POST['password'])){
            if(Auth::_getInstance() -> authorize($_POST['email'], $_POST['password'])){
                HTTP::Redirect(URLABS.'manage/');
            }
        }
    }
    
    public function outAction(){
        Auth::_getInstance() -> logout();
        HTTP::Redirect(URLABS.'manage/login/');
    }

}
