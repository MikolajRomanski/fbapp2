<?php

class mainpageClass{
    /**
     *
     * @var libraryLog
     */
    private $log;
    
    /**
     *
     * @var UseSmarty
     */
    private $smarty;
    public function __construct(){
        $this -> log = libraryLog::_getInstance();
        $this -> smarty = UseSmarty::getInstance();
        if(!Auth::_getInstance() ->is_auth()){
            HTTP::Redirect(URLABS.'manage/login/');
        }
         $this -> smarty = UseSmarty::getInstance();
        $js = new objJS();
        $js->add('main.js', 'js/panel');
        $js->add('jquery.prettyPhoto.js', 'js/panel');
        $this->smarty->assign('additionaljs', $js->generate());

        $css = new objCSS();
        $css -> add('prettyPhoto.css', 'css/panel/');
        $generate = $css ->generate();
        $this -> log -> info('css', $generate);
        $this->smarty->assign('additionalcss', $generate);
        
        
    }
    public function startAction(){
        $users = goldbachApi::_getInstance() -> getFBUsers();
        $this -> smarty -> assign('users', $users);
    }
    
    
    public function listelementsAction(){
        $user = new UserData($_GET['id1']);
        
        $info = $user -> getUserInformations();
        $this -> smarty ->assign('user', $info);
        
        $elements = $user ->get_user_elements();
        $this -> smarty -> assign('elements', $elements);
        
        $facebook = $user -> getFacebookData($info['info']['userid']);
        $this -> smarty ->assign('fbdata', print_r($facebook, true));
    }
    
    public function listresourcesAction(){
        $user = new UserData();
        
        $userid = $user -> getUserIdFromElement($_GET['id1']);
        $user = new UserData($userid);
        
        if(!empty($_POST['changesettings'])) {
            $status = $_POST['visible'];
            $description = $_POST['description'];
            $dane['status'] = $status;
            $dane['description'] = $description;
            $user ->update_element($_GET['id1'], $dane);
        }
        
        
        $info = $user -> getUserInformations();
        $this -> smarty ->assign('user', $info);
        
        $facebook = $user -> getFacebookData($info['info']['userid']);
        $this -> smarty ->assign('fbdata', print_r($facebook, true));
        
        $element = $user -> get_element($_GET['id1']);
        $this -> smarty -> assign('element', $element);
        
        $this -> smarty -> assign('ready', file_exists(RESOURCES.$userid.'/'.$element['id'].'_ready.png'));
        
        $pictures = $user ->get_pictures_from_element($_GET['id1'], 'ASC');
        $this -> smarty -> assign('pictures', $pictures);
        
        
    }
    
};