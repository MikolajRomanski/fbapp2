<?php
class usersClass{
    /**
     *
     * @var libraryLog
     */
    private $log;
    
    /**
     *
     * @var UseSmarty
     */
    private $smarty;
    public function __construct(){
        $this -> log = libraryLog::_getInstance();
        $this -> smarty = UseSmarty::getInstance();
        if(!Auth::_getInstance() ->is_auth()){
            HTTP::Redirect(URLABS.'manage/login/');
        }
         $this -> smarty = UseSmarty::getInstance();
        $js = new objJS();
        $js->add('main.js', 'js/panel');
        $js->add('jquery.prettyPhoto.js', 'js/panel');
        $this->smarty->assign('additionaljs', $js->generate());

        $css = new objCSS();
        $css -> add('prettyPhoto.css', 'css/panel/');
        $generate = $css ->generate();
        $this -> log -> info('css', $generate);
        $this->smarty->assign('additionalcss', $generate);
        
    }
    public function listAction(){
        
    }
}
?>