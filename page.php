<?php

ob_start();
require_once 'base/config.inc.php';

define('URLABS', ( (isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && $_SERVER["HTTP_X_FORWARDED_PROTO"] == "https") || ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on" ) ) ? 'https://' . SITE_URI : 'http://' . SITE_URI);


$offset = -60 * 60 * 24;
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", 0) . " GMT";
header($ExpStr);
header("Cache-Control: no-store, no-cache");
header("Cache-Control: no-cache");
header("Cache-Control: no-store");
header("Pragma: no-cache");
header('Last-Modified: ' . date("r", time()));

//includy wszystkich klas i bibliotek
$dir = opendir('_class');
while ($plik = readdir($dir)) {
    if (!is_dir('_class/' . $plik))
        require_once '_class/' . $plik;
}
closedir($dir);
$dir = opendir('_library');
while ($plik = readdir($dir)) {
    if (!is_dir('_library/' . $plik))
        require_once '_library/' . $plik;
}
closedir($dir);

$ben = Benchmark::_getInstance();
$ben_ = $ben->Start('Obsługa wywołania');

libraryLog::_getinstance()->group_start('Variables', '', true);
libraryLog::_getinstance()->log('GET', $_GET);
libraryLog::_getinstance()->log('POST', $_POST);
libraryLog::_getinstance()->log('REQUEST', $_REQUEST);
$_POST = Translate::make_safe_array_post($_POST);
$_GET = Translate::make_safe_array_get($_GET);

libraryLog::_getinstance()->log('_FILE', $_FILES);
libraryLog::_getinstance()->log('_SESSION', $_SESSION);
libraryLog::_getinstance()->group_end();

$class = Translate::make_filename($_GET['class']);
$method = Translate::make_filename($_GET['action']);
if (!empty($_SESSION['pagetab_redirect'])) {
    $class = $_SESSION['pagetab_redirect']['class'];
    $method = $_SESSION['pagetab_redirect']['method'];
    unset($_SESSION['pagetab_redirect']);
}
//save variables between redirects
define('objectclass', $class);
define('objectmethod', $method);

$userinfo = array();
if ($class == 'mainpage' && $method == 'start') {
    $facebook = Fcb::getInstance();
    $userinfo = $facebook->getUserData();
    if (!empty($userinfo)) {
        $facebook->auth_workaround($userinfo);
    }
}

if (!file_exists('modules/' . $class . '.php')) {
    libraryLog::_getInstance()->error('brak modułu', 'modules/' . $class . '.php');
    die('object required not found!');
}

$templatename = 'm_' . $class . '_' . $method . '.tpl';
$smarty = UseSmarty::getInstance();
$smarty->assign('class', $class);
$smarty->assign('appId', appId);
$smarty->assign('FBURL', FANPAGE_APP);
$smarty->assign('FANPAGE_APPTAB', FANPAGE_APP);
$smarty->assign('APPURL', APPURL);
$smarty->assign('FBPROFILE2LIKE_URL', FBPROFILE2LIKE_URL);

$smarty->assign('pagetitle', '');
$smarty->assign('actm', '');


$smarty->assign('additionaljs', '');
$smarty->assign('additionalcss', '');

$smarty->assign('authurl', '');

$smarty->assign('abs', URLABS);

$smarty->assign('rawabs', SITE_URI);

//$smarty->assign('s', $s);
//libraryLog::_getInstance() -> info('URLABS', $_SERVER['HTTPS']);
libraryLog::_getInstance()->info('URLABS', URLABS);
$smarty->assign('abs', URLABS);
$smarty->assign('template', $templatename);

require_once 'modules/' . $class . '.php';
$objname = $class . 'Class';
$obj = new $objname;
$methodname = $method . 'Action';

$ben_2 = $ben->Start('Obsługa modułu');
if (!method_exists($obj, $methodname))
    die('object method not found!');
else
    $obj->$methodname();
$ben->End($ben_2);

$js = objJS::getInstance();
$css = objCSS::getInstance();


$js->add('jquery.min.js');
$js->add('jquery.tipsy.js');
$js->add('jquery.cookie.js');
$js->add('jquery.easing.js');
$js->add('jquery.color.js');
$js->add('jquery.Jcrop.js');
$js->add('jquery.iframe-post-form.js');
$js->add('custom-form-elements.js');



$js->add('main.js');


$css->add('reset.css');
$css->add('fontface.css');
$css->add('tipsy.css');
$css->add('jquery.Jcrop.css');
$css->add('form.css');

$css->add('style.css');

$js->add('jquery-transit.min.js');




$js_ = $js->generate();
$smarty->assign('js', $js_);
$css_ = $css->generate();
$smarty->assign('css', $css_);



if(is_numeric(ini_get('upload_max_filesize'))) {
    libraryLog::_getInstance() -> info('MAX POST INT' , ini_get('upload_max_filesize'));
    
}
else {
    libraryLog::_getInstance() -> warn('MAX POST STRING' , ini_get('upload_max_filesize'));
    
}

libraryLog::_getInstance() -> warn('MAX MEMORY_LIMIT' , ini_get('memory_limit'));




libraryLog::_getInstance()->info('template', $smarty->get_template_vars('template'));
if ($_GET['dynamic'] == 0) {
    $content = $smarty->fetch('_main.tpl');
} else {
    $content = $smarty->fetch($templatename);
}

$errspam = ob_get_contents(); /*
  libraryLog::_getInstance()->group_start('translating ' . count($_SESSION['translate']), 'DDAA11');
  libraryLog::_getInstance()->info('translateels', $_SESSION['translate']);
  libraryLog::_getInstance()->log('translatestr', implode("\n", $_SESSION['translate']));
  libraryLog::_getInstance()->group_end();
 */
$ben->End($ben_2);
ob_end_clean();
if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
    ini_set('zlib.compresss_level', 9);
    ob_start("ob_gzhandler");
} else {
    ob_start();
}

$errspam = trim($errspam);
if ($errspam != '')
    libraryLog::_getinstance()->warn('Module printed', $errspam);

//wyświetl sparsowany szablon .tpl
//bad boy... make it better

if (COMPRESS_CODE) {
    $size_before = strlen($content);
    $search = array(
        '/\>[^\S ]+/s', //strip whitespaces after tags, except space
        '/[^\S ]+\</s', //strip whitespaces before tags, except space
        '/(\s)+/s'  // shorten multiple whitespace sequences
    );
    $replace = array(
        '>',
        '<',
        '\\1'
    );
    $content = preg_replace($search, $replace, $content);
    $size_after = strlen($content);
    $dif = $size_before - $size_after;
//brutal
    libraryLog::_getinstance()->info('Compress', Files::getfilesize($size_before) . ' - ' . Files::getfilesize($size_after) . ' = ' . Files::getfilesize($dif));
}
echo $content;
//end of dirty bad boy code
//echo $content;



$ben->End($ben_);
$ben->Raport();
ob_end_flush();
?>
