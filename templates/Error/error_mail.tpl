<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <style>
        </style>
    </head>
    <body style="font-family: 'Bitstream Vera Sans','DejaVu Sans','Liberation Sans',Kalimati,Verdana,Geneva,sans-serif; font-size: 11px; color: #696969; line-height: 140%">
        <table style="width: 600px;">
            <tr><td><img src="pagestyles/img/error.jpg"></td></tr>
            <tr><td>Raport błędu strony <a href="{$abs}" style="color: #708EA0;">{$abs}</a></td></tr>
            <tr><td style="font-size: 11px !important;"><b>{$time}</b><br/><b>{$server.REMOTE_ADDR}</b><br/>(<i>{$server.HTTP_USER_AGENT|stripslashes|escape}</i>)</td></tr>
            <tr><td style="font-size: 11px !important; background-color: #EDEDED;">Wiadomość wysłana do:<br/><ul>{foreach from=$developers_mails item=poz}<li>{$poz}</li>{/foreach}</ul></td></tr>

            <tr><td style="font-size: 11px !important; height: 20px; font-weight: bold; background-color: #EADEE7;">ERROR</td></tr>
            <tr><td style="font-size: 11px !important;">{$error.file} <i>(linia: #{$error.line})</i><br/>{$error.message}</td></tr>
            <tr><td style="font-size: 11px !important; height: 20px; font-weight: bold; background-color: #EADEE7;">REQUEST</td></tr>
            <tr><td style="font-size: 11px !important;">
                    <b>REQUEST_URI:</b> {$server.REQUEST_URI}<br/>
                    <b>HTTP_REFERER</b> {$server.HTTP_REFERER}<br/>
                    <b>QUERY_STRING:</b> {$server.QUERY_STRING}<br/>
                    <b>REQUEST_METHOD:</b> {$server.REQUEST_METHOD}<br/>
                    <b>DOCUMENT_ROOT:</b> {$server.DOCUMENT_ROOT}<br/>
                    <b>Plik</b> {$server.SCRIPT_FILENAME}<br/>
                    <b>Redirect</b> {$server.REDIRECT_URL}
                </td></tr>
            <tr><td style="font-size: 11px !important; height: 20px; font-weight: bold; background-color: #EADEE7;">POST</td></tr>
            <tr><td style="font-size: 11px !important;"><pre>{$post}</pre>
            <tr><td style="font-size: 11px !important; height: 20px; font-weight: bold; background-color: #EADEE7;">GET</td></tr>
            <tr><td style="font-size: 11px !important;"><pre>{$get}</pre></td></tr>
        </table>
    </body>
</html>