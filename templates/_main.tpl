<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" dir="ltr" lang="pl-PL">
    <head profile="http://gmpg.org/xfn/11">
        <base href="{$abs}" id="base_url" />
        <meta http-equiv="Cache-Control" content="no-cache"/> 
        <meta http-equiv="Pragma" content="public"/>
        <meta property="og:title" content="Podróż za jeden uśmiech"/>
        <meta property="og:url" content="{$APPURL}" />
        <meta property="og:description" content="Podróż za jeden uśmiech" />
        <meta property="og:image" content="{$abs}bigico.png" />
        <meta property="og:appId" content="{$appId}" />


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <script type="text/javascript">
            var abs = "{$abs}";
            var fburl = "{$FBURL}";
            var fbtab = "{$FANPAGE_APPTAB}";
            var cla="{$class}";
            var lang = "pl";
            var appurl = "{$APPURL}";
            actelement = '';
        </script>
        {if $authurl!=''}
            <script type="text/javascript">
                document.location.href='{$authurl}';
                parent.location.href='{$authurl}';
            </script>
        {/if}
    {foreach from=$css item=poz}<link rel="stylesheet" href="css/{$poz}" type="text/css" media="screen" charset="utf-8" />{/foreach}

    {if $additionalcss!=''}
    {foreach from=$additionalcss item=poz}<link rel="stylesheet" href="css/{$poz}" type="text/css" media="screen" charset="utf-8" />{/foreach}
{/if}


<!--[if IE 8]>
    <style type="text/css" media="screen">
        @import url(css/iehacks8.css);
    </style>
    <![endif]-->

<!--[if IE 9]>
    <style type="text/css" media="screen">
        @import url(css/iehacks9.css?{$smarty.now});
    </style>
    <![endif]-->

<!--[if IE 10]>
    <style type="text/css" media="screen">
        @import url(css/iehacks10.css);
    </style>
    <![endif]-->

</head>
<body>
    <div class="maincontainer">
        <div id="fb-root"></div>
        {include file="js_fb_javascript.tpl"}
        <div class="midcnt">
            <div class="mt">{include file="i_menu_top.tpl"}</div>
            <div class="middlebg"></div>
            <div class="middle">
                <div class="clear1"></div>
                <p class="kamilllink"><a href="http://www.kamill.pl/" target="_blank"></a></p>
                <p class="info1" style="visibility: hidden;">
                    Nagroda główna:<br/>
                    bon Travel Pass Spa<br/>
                    o wartości 1 500 zł</p>
                <p class="info2">To, co dla nas najważniejsze wyraża dotyk...<br/>
                    Wystarczy, że dotkniesz swojego aparatu,<br/>
                    a wyjazd do SPA z Kamill <br/>
                    może być w zasięgu Twojej ręki!
                </p>
            </div>
        </div>
        <div class="tcnt">
        {if $template!='m_mainpage_start.tpl'}<div id="topgal" class="tempheader"><h1>{$pagetitle}</h1></div>{/if}
        <div class="clear1"></div>
        {include file=$template}
        <div class="clear1"></div>
    </div>
    <div class="clear"></div>
    {include file="i_footer.tpl"}
    <div class="reg">
        <p>Ta promocja nie jest sponsorowana, administrowana ani organizowana przez Facebook. Dołączając do konkursu przekazujesz swoje prywatne informacje organizatorowi.Informacje te zostaną wykorzystane tylko do celów konkursu. </p>
    </div>
{foreach from=$js item=poz}<script type="text/javascript" src="{$poz}" ></script>{/foreach}
</div>       
</body>
</html>
