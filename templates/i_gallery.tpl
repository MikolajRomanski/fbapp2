<ul class="glist">{foreach from=$galleries item=poz}
    <li>
        <div class="ipreview" style="background-image: url('resources/{$poz.systemid}/{$poz.id}_ready.png');"></div>
        <div class="pdesc">
            <p class="name">galeria usciskow <br/>
                {$poz.first_name|stripslashes|escape} {$poz.last_name|truncate:2:'.'|stripslashes|escape}</p>
            
            <p class="rdesc">{$poz.description|stripslashes|escape|nl2br}</p>
            <p class="rx"></p>
            <div class="clear"></div>
        </div>
    </li>
{/foreach}
</ul>