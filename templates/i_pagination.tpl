<div class="pagcnt">
    {if $paginate.links|@count>1}
        <ul class="upaginate">
            <li class="lpaginate">
                <a href="{$paginate.first}#topgal" class="dynload">
                    <img src="/css/img/lpoint.png" alt="pierwsza"/>
                </a>
            </li>
            {if is_array($paginate.links)}
                {foreach from=$paginate.links item=poz key=key}
                    <li class="lpaginate {if $active_page==$key}activepage{/if}">
                        <a href="{$poz}#topgal" class="img_link ">{$key}</a>
                    </li>
                {/foreach}
            {/if}
            <li class="lpaginate">
                <a href="{$paginate.last}#topgal" class="dynload">
                    <img src="/css/img/rpoint.png" alt="ostatnia"/>
                </a>
            </li>
        </ul>
    {/if}
    <div class="clear"></div>
</div>
<div class="clear"></div>