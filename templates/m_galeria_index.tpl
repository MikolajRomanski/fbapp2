<div class="menav"><div class="clear1"></div>
    <ul>
        <li><a href='/info/start/#topgal'>Przeglądaj</a></li>
        <li class="wx"></li>
        <li><a href='galeria/#topgal'>Utwórz własny kolaż</a></li>
    </ul>
</div>
<div class="point">
    <ul>
        <li><a onclick="loadcnt('step1', '');">Wybierz szablon</a></li>
        <li><a onclick="showtemplateupload();">Wgraj zdjęcia</a></li>
        <li><a rel="" style='cursor: default;'>Uzupełnij opis</a></li>
    </ul>
</div>
<div class="cntslide">
    <div class="step1">
        <div class="pagetitle">
            <p class="left">1.</p>
            <h2>Z ilu zdjęć tworzysz kolaż ?</h2>
        </div>
        <div class="templateselector">
            <a class="picturecount" onclick="loadcnt('step2', 3);">3<input type="hidden" value="3"/></a><a class="picturecount" onclick="loadcnt('step2', 4);">4<input type="hidden" value="4"/></a><a class="picturecount mr0" onclick="loadcnt('step2', 5);">5<input type="hidden" value="5"/></a>
        </div>
    </div>
</div>
<div id="step1"class="vhid">
    <div class="step1">
        <div class="pagetitle">
            <p class="left">1.</p>
            <h2>Z ilu zdjęć tworzysz kolaż ?</h2>
        </div>
        <div class="templateselector">
            <a class="picturecount" onclick="loadcnt('step2', 3);">3<input type="hidden" value="3"/></a><a class="picturecount" onclick="loadcnt('step2',4);">4<input type="hidden" value="4"/></a><a class="picturecount mr0" onclick="loadcnt('step2',5);">5<input type="hidden" value="5"/></a>
        </div>
    </div>
</div>


<div id="step3"class="vhid">
    <div class="step3">
        <div class="pagetitle">
            <p class="left">3.</p>
            <h2>Uzupełnij opis/ <span class="small">500 znakow</span></h2>
        </div>
        <div class="desccnt">
            <div class="clear1"></div>
            <p class="x"></p>
            <p class="name">galeria usciskow <br/>
                {$userdata.first_name|stripslashes|escape} {$userdata.last_name|truncate:2:'.'|stripslashes|escape}</p>
            
            <div class="textarea">
                <textarea style="background: transparent url('css/img/pencil.png') no-repeat 0 0;"></textarea>
            </div>
            <div class="clear1"></div>
        </div>
        <div class="clear1"></div>
        <div class="acreg" rel="Aby kontynuować, musisz zakceptować regulamin"><input type="checkbox"  class="styled" id="accreg"/><label for="accreg"><p>Akceptuję <a href="Regulamin_konkursu_podrozzauscisk_25.04.pdf" class="blank reglink">regulamin</a></p></label></div>
        <p class="getuse share" rel="Zaakceptuj regulamin, aby przejść dalej"><a onclick="submitfile();"><span>weź udział w konkursie</span><span class="img"><img src="css/img/arr.png" alt=""/></span></a></p>
    </div>
</div>

<div id="step4"class="vhid">
    <div class="step4">
        <div class="pagetitle">
            <p class="left">3.</p>
            <h2>Dziękujemy</span></h2>
            <div class="clear1"></div>
            <div class="backlink"><a class="dblock" href="/"><span>Wróć</span></a></div>
        </div>
    </div>
</div>


<div id="step2" class="vhid">
    <div class="els_3">
        
        <div class="step2">
            <div class="pagetitle">
                <p class="left">2.</p>
                <h2>Wybierz szablon kolażu</h2>
            </div>
            <p class="getuse nagr savecontentnow hidden"><a onclick="savecontentnow();"><span>Zapisz kolaż</span><span class="img"><img src="css/img/arr.png" alt=""/></span></a></p>
            <div class="pictmpl">
                <div class="description">
                    <p>1. Wybierz ramkę i do każdej z ramek wgraj zdjęcie, które ma się pojawić w kolażu</p>
                    <p>2. Za pomocą myszki zaznacz tę część zdjęcia, która ma być widoczna w kolażu</p>
                </div>
                <div class="previewcontainer pic3cn">
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                </div>
                <div id="pic32" class="ps">
                    <div class="ccont ccont1"><input type="hidden" value="1"/></div>
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                </div>
                <div id="pic33" class="ps">
                    <div class="ccont ccont2"><input type="hidden" value="2"/></div>
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                </div>
                <div id="pic34" class="ps">
                    <div class="ccont ccont3"><input type="hidden" value="3"/></div>
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                </div>
                <div id="pic35" class="ps act">
                    <div class="ccont ccont4"><input type="hidden" value="4"/></div>
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                </div>
                <div class="clear1"></div>
                <div class="description hidden">
                    <p>1. Wybierz ramkę i do każdej z ramek wgraj zdjęcie, które ma się pojawić w kolażu</p>
                    <p>2. Za pomocą myszki zaznacz tę część zdjęcia, która ma być widoczna w kolażu</p>
                </div>
            </div>
            <p class="getuse nagr savecontentnow hidden"><a onclick="savecontentnow();"><span>Zapisz kolaż</span><span class="img"><img src="css/img/arr.png" alt=""/></span></a></p>
        </div>
        
    </div>
    <div class="els_4">
        
        <div class="step2">
            <div class="pagetitle">
                <p class="left">2.</p>
                <h2>Wybierz szablon kolażu</h2>
            </div>
            <p class="getuse nagr savecontentnow hidden"><a onclick="savecontentnow();"><span>Zapisz kolaż</span><span class="img"><img src="css/img/arr.png" alt=""/></span></a></p>
            <div class="pictmpl">
                <div class="description">
                    <p>1. Wybierz ramkę i do każdej z ramek wgraj zdjęcie, które ma się pojawić w kolażu</p>
                    <p>2. Za pomocą myszki zaznacz tę część zdjęcia, która ma być widoczna w kolażu</p>
                </div>
                <div class="previewcontainer pic4cn">
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                    <div class="pict4"></div>
                </div>
                <div id="pic42" class="ps">
                    <div class="ccont ccont1"><input type="hidden" value="1"/></div>
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                    <div class="pict4"></div>
                </div>
                <div id="pic43" class="ps">
                    <div class="ccont ccont2"><input type="hidden" value="2"/></div>
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                    <div class="pict4"></div>
                </div>
                <div id="pic44" class="ps">
                    <div class="ccont ccont3"><input type="hidden" value="3"/></div>
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                    <div class="pict4"></div>
                </div>
                <div id="pic45" class="ps act">
                    <div class="ccont ccont4"><input type="hidden" value="4"/></div>
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                    <div class="pict4"></div>
                </div>
                <div class="clear1"></div>
                <div class="description hidden">
                    <p>1. Wybierz ramkę i do każdej z ramek wgraj zdjęcie, które ma się pojawić w kolażu</p>
                    <p>2. Za pomocą myszki zaznacz tę część zdjęcia, która ma być widoczna w kolażu</p>
                </div>
            </div>
        </div>
        
    </div>
    <div class="els_5">
        
        <div class="step2">
            <div class="pagetitle">
                <p class="left">2.</p>
                <h2>Wybierz szablon kolażu</h2>
            </div>
            <p class="getuse nagr savecontentnow hidden"><a onclick="savecontentnow();"><span>Zapisz kolaż</span><span class="img"><img src="css/img/arr.png" alt=""/></span></a></p>
            <div class="pictmpl">
                <div class="description">
                    <p>1. Wybierz ramkę i do każdej z ramek wgraj zdjęcie, które ma się pojawić w kolażu</p>
                    <p>2. Za pomocą myszki zaznacz tę część zdjęcia, która ma być widoczna w kolażu</p>
                </div>
                <div class="previewcontainer pic5cn">
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                    <div class="pict4"></div>
                </div>
                <div id="pic52" class="ps">
                    <div class="ccont ccont1"><input type="hidden" value="1"/></div>
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                    <div class="pict4"></div>
                    <div class="pict5"></div>
                </div>
                <div id="pic53" class="ps">
                    <div class="ccont ccont2"><input type="hidden" value="2"/></div>
                    <div class="pict1"></div>
                    <div class="pict2"></div>
                    <div class="pict3"></div>
                    <div class="pict4"></div>
                    <div class="pict5"></div>
                </div>
                <div class="clear1"></div>
                <div class="description hidden">
                    <p>1. Wybierz ramkę i do każdej z ramek wgraj zdjęcie, które ma się pojawić w kolażu</p>
                    <p>2. Za pomocą myszki zaznacz tę część zdjęcia, która ma być widoczna w kolażu</p>
                </div>
            </div>
        </div>
        
    </div>

</div>
<div id="pcont">
    <script type="text/javascript">
        var actelement = '{$elements[0].additional_data.elements}';
        var templatetype = '{$elements[0].additional_data.templatetype}';
        
        var pictures = new Array();
        pictures['pict1'] = new Array();
        pictures['pict2'] = new Array();
        pictures['pict3'] = new Array();
        pictures['pict4'] = new Array();
        pictures['pict5'] = new Array();
        {if $pictures!=''}
            {foreach from=$pictures item=poz}
                
            {/foreach}
        {/if}
        
    </script>
</div>