<ul class="pcen">
    <li>
        <p>1. Zrób serię zdjęć pokazujących<br/> najcenniejsze momenty,<br/>jakie przydarzyły się Twoim dłoniom jednego dnia</p>
    </li>
    <li>
        <p>2. Wybierz najlepsze i dodaj do aplikacji,<br/> która utworzy z nich barwny kolaż</p>
    </li>
    <li>
        <p>
            <span class="red">Uwaga!<br/>Przynajmniej na jednym zdjęciu powinien<br/> znaleźć się uścisk dłoni</span>
        </p>
    </li>
    <li>
        <p>3. Dodaj tytuł do swojego kolażu</p>
    </li>
    <li class="nobg">
        <p>4. Zaproś znajomych do głosowania na swoje zdjęcia</p>
    </li>
</ul>
<p class="getuse" style="margin-top: 40px; margin-bottom: 40px; text-align: center; font-size: 15px; cursor: default;">
Podróż za jeden uśmiech dobiegła końca!<br/>
Zwycięzców konkursu ogłosimy do 17 maja 2013.<br/>
Teraz wszystko w rękach naszego jury.<br/>
Trzymajcie kciuki - za siebie i swoich znajomych!<br/>
</p>