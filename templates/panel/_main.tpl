<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" dir="ltr" lang="pl-PL">
    <head profile="http://gmpg.org/xfn/11">
        <base href="{$abs}" id="base_url" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Kamill System</title>
        {foreach from=$css item=poz}
            <link rel="stylesheet" href="css/{$poz}" type="text/css" media="screen" charset="utf-8" />
        {/foreach}

        {if $additionalcss!=''}
            {foreach from=$additionalcss item=poz}
                <link rel="stylesheet" href="css/{$poz}" type="text/css" media="screen" charset="utf-8" />
            {/foreach}
        {/if}
        <script type="text/javascript">
            var abs = "{$abs}";
        </script>
    </head>
    <body>
        {if $class!='login'}
            {include file="i_menu.tpl"}
            <div class="content blur">
                <div class="cnt2">
                    {include file=$template}
                </div>
            </div>
        {else}
            
            {include file=$template}
        {/if}

    {foreach from=$js item=poz}<script type="text/javascript" src="{$poz}" ></script>{/foreach}
    {if $additionaljs!=''}
        {foreach from=$additionaljs item=poz}
            <script type="text/javascript" src="{$poz}" ></script>
        {/foreach}
    {/if}
</body>
</html>