<h1><a href="manage/mainpage/listelements/{$user.info.id}">{$user.info.first_name|stripslashes|escape} {$user.info.last_name|stripslashes|escape}</a></h1>
<div class="standardinfo">
    <table>
        <tr>
            <td>Username</td><td><a href="http://facebook.com/" target="_blank">{$user.info.username|stripslashes|escape}</a></td>
        </tr>
        <td>Imię</td><td>{$user.info.first_name|stripslashes|escape}</td>
        </tr>
        <td>Nazwisko</td><td>{$user.info.last_name|stripslashes|escape}</td>
        </tr>
        <td>Email</td><td>{$user.info.email|stripslashes|escape}</td>
        </tr>
    </table>

</div>
<div class="fbdata">
    <pre>
{if $fbdata!=''}{$fbdata|stripslashes|escape}{else}brak danych. Możliwe, że access token wygasł/jest nieprawidłowy{/if}
</pre>
</div>

<div class="clear"></div>
<div class="manage">
    <form method="post" action=""/>
    <p>
        <label>Status: </label><br/>
        <select name="visible">
            <option value="0"{if $element.status==0}selected="selected"{/if}>Niewidoczny</option>
            <option value="1"{if $element.status==1}selected="selected"{/if}>Widoczny</option>
        </select>
    </p>
    <p>
        <label>Opis: </label><br/>
        <textarea name="description">{$element.description|stripslashes|escape}</textarea>
    </p>
    <p><input type="submit" name='changesettings' value="Zapisz zmiany" style="margin-top: 20px;"/></p>
</div>
</div><div class="clear1"></div>
{if $element==''}
    <h2 class="mt50">Wybrany element nie istnieje
    {else}
        {if $ready}
            <img class="ready" src="resources/{$user.info.id}/{$element.id}_ready.png" alt=""/>
        {/if}
        <div class="clear1"></div>
        {if $pictures!=''}
            <ul class="picturelist">
                {foreach from=$pictures item=poz}
                    <li style="background-image: url('resources/{$user.info.id}/{$poz.id}.image.png');"><a href="{$poz.path}" rel="gal" class="prettyPhoto iimg" title="Dodano: {$poz.created|date_format:"%Y-%m-%d %H:%M:%S"}, wielkość pliku: {$poz.size|humansize}"}"></a></li>
                {/foreach}
            </ul>
        {/if}
    {/if}
    <div class="clear" style="height: 20px;"></div>
    <a href="manage/mainpage/listelements/{$user.info.id}">wstecz</a>
    <div class="clear" style="height: 20px;"></div>